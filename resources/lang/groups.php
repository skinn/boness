<?php
return [
    'header' => [
        'group' => 'Header',
        'sub_title' => '......'
    ],
    'content' => [
        'group' => 'Inhoud',
        'sub_title' => 'Inhoud wijzigen of toevoegen'
    ],


    'spotlight' => [
        'group' => 'In de kijker',
        'sub_title' => '......'
    ],
    'cta' => [
        'group' => 'Oproep tot actie',
        'sub_title' => 'Dit blok verzoekt de bezoeker om contact op te nemen'
    ],


    'blog' => [
        'group' => 'Blog',
        'sub_title' => 'Lijst van de blogberichten. Voeg nieuwe toe of wijzig bestaande. '
    ],
    'blog_detail' => [
        'group' => 'Blogbericht',
        'sub_title' => 'Inhoud van het blogbericht'
    ],
    'news' => [
        'group' => 'Nieuws',
        'sub_title' => 'Lijst van de nieuwsberichten. Voeg nieuwe toe of wijzig bestaande. '
    ],
    'news_detail' => [
        'group' => 'Nieuws',
        'sub_title' => 'Inhoud van het nieuwsbericht'
    ],
    'activities' => [
        'group' => 'Activiteiten',
        'sub_title' => 'Lijst van de activiteiten. Voeg nieuwe toe of wijzig bestaande. '
    ],
    'activity_detail' => [
        'group' => 'Activiteit',
        'sub_title' => 'Inhoud van de activiteit'
    ],
    'faq' => [
        'group' => 'FAQ',
        'sub_title' => 'Lijst van de veelgestelde vragen & antwoorden'
    ],


    'contact' => [
        'group' => 'Contactgegevens',
        'sub_title' => 'Adresgegevens, Telefoon, Fax, Social media links, ...'
    ],
    'contact_form' => [
        'group' => 'Contactformulier',
        'sub_title' => 'Bekijk de inzendingen van het contactformulier',
    ],
    'newsletter_form' => [
        'group' => 'Nieuwsbriefformulier',
        'sub_title' => 'Bekijk de inschrijvingen op de nieuwsbrief',
    ],
    'files' => [
        'group' => 'Bestanden',
        'sub_title' => 'Upload hier PDFs en kopieer de links om in teksten te gebruiken'
    ],


    'seo' => [
        'group' => 'SEO instellingen',
        'sub_title' => 'Optimaliseer de weergave in Google, Facebook, ...'
    ],
];
