<?php

return [

    'labels' => [
        'asset' => 'asset',
        'boolean' => 'checkbox',
        'date-time' => 'date-time',
        'list' => 'list (dropdown, checkboxes, list)',
        'numeric' => 'numeric',
        'text' => 'text',
        'text_type' => [
            'single_line' => 'single line',
            'multi_line' => 'html editor (no styling)',
            'html' => 'html editor (no paragraphs)',
            'html_full' => 'html editor (full)',
            'slug' => 'slug'
        ],
        'numeric_type' => [
            'integer' => 'integer',
            'decimal' => 'decimal',
            'position' => 'position'
        ],
        'datetime_type' => [
            'date' => 'date',
            'date_time' => 'date and time'
        ],
        'datetime_default' => [
            'date' => 'date input',
            'now' => 'current date (and time)'
        ],
        'selection_type' => [
            'dropdown' => 'dropdown',
            'checkboxes' => 'checkboxlist',
            'list' => 'datalist'
        ],
        'dropdown_type' => [
            'static' => 'static',
            'dynamic' => 'dynamic'
        ]
    ],

];
