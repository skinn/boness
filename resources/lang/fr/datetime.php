<?php
return [
    'day_of_week' => [
        'long' => [
            '01' => 'lundi',
            '02' => 'mardi',
            '03' => 'mercredi',
            '04' => 'jeudi',
            '05' => 'vendredi',
            '06' => 'samedi',
            '07' => 'dimanche'
        ],
        'short' => [
            '01' => 'lun',
            '02' => 'mar',
            '03' => 'mer',
            '04' => 'jeu',
            '05' => 'ven',
            '06' => 'sam',
            '07' => 'dim'
        ]
    ],
    'month' => [
        'long' => [
            '01' => 'janvier',
            '02' => 'février',
            '03' => 'mars',
            '04' => 'avril',
            '05' => 'mai',
            '06' => 'juin',
            '07' => 'juillet',
            '08' => 'août',
            '09' => 'septembre',
            '10' => 'octobre',
            '11' => 'novembre',
            '12' => 'décembre'
        ],
        'short' => [
            '01' => 'janv',
            '02' => 'fevr',
            '03' => 'mars',
            '04' => 'avril',
            '05' => 'mai',
            '06' => 'juin',
            '07' => 'juil',
            '08' => 'août',
            '09' => 'sept',
            '10' => 'oct',
            '11' => 'nov',
            '12' => 'déc'
        ]
    ]
];