<?php
return [
    'day_of_week' => [
        'long' => [
            '01' => 'maandag',
            '02' => 'dinsdag',
            '03' => 'woensdag',
            '04' => 'donderdag',
            '05' => 'vrijdag',
            '06' => 'zaterdag',
            '07' => 'zondag'
        ],
        'short' => [
            '01' => 'maa',
            '02' => 'din',
            '03' => 'woe',
            '04' => 'don',
            '05' => 'vri',
            '06' => 'zat',
            '07' => 'zon'
        ]
    ],
    'month' => [
        'long' => [
            '01' => 'januari',
            '02' => 'februari',
            '03' => 'maart',
            '04' => 'april',
            '05' => 'mei',
            '06' => 'juni',
            '07' => 'juli',
            '08' => 'augustus',
            '09' => 'september',
            '10' => 'oktober',
            '11' => 'november',
            '12' => 'december'
        ],
        'short' => [
            '01' => 'jan',
            '02' => 'feb',
            '03' => 'mar',
            '04' => 'apr',
            '05' => 'mei',
            '06' => 'jun',
            '07' => 'jul',
            '08' => 'aug',
            '09' => 'sep',
            '10' => 'okt',
            '11' => 'nov',
            '12' => 'dec'
        ]
    ]
];