<?php
return [
    'groups' => file_exists(base_path('/resources/lang/groups.php')) ? include(base_path('/resources/lang/groups.php')) : [],
    'mails' => file_exists(base_path('/resources/lang/mails.php')) ? include(base_path('/resources/lang/mails.php')) : [],
    'labels' => [
        '404' => 'Pagina niet gevonden.',
        'about' => 'Over ons',
        'all-categories' => 'Alle categorieën',
        'all-types' => 'Alle types',
        'blog' => 'Blog',
        'city' => 'Stad',
        'company' => 'Bedrijf',
        'compare' => 'Vergelijk',
        'contact' => 'Contact opnemen',
        'email' => 'E-mail',
        'email-required' => 'Gelieve een geldig e-mail adres in te vullen',
        'first-name' => 'Naam',
        'first-name-required' => 'Gelieve uw voornaam in te vullen',
        'interest' => 'Ik ben geïnteresseerd in',
        'jobs' => 'Jobs',
        'last-name' => 'Familienaam',
        'last-name-required' => 'Gelieve uw naam in te vullen',
        'message-required' => 'Gelieve een bericht achter te laten',
        'name' => 'Naam',
        'opening-times' => 'Openingsuren',
        'option' => 'Gelieve een optie te selecteren.',
        'order' => 'Bestel',
        'phone' => 'Telefoon',
        'photos' => 'Foto\'s',
        'question' => 'Vraag',
        'required' => '(*) Verplichte velden',
        'send' => 'Verzenden',
        'services' => 'Diensten',
        'share' => 'Delen',
        'street' => 'Straat',
        'subject' => 'Onderwerp bericht:',
        'travel-directions' => 'Routebeschrijving',
        'under-construction' => 'Deze pagina is in opbouw',
        'to-homepage' => 'Naar de homepagina',
    ],
    'menu' => [
        'home' => 'home',
        'blog' => 'blog',
        'contact' => 'contacteer ons',
    ],
    'messages' => [
        'contact-thx-msg' => 'Bedankt voor uw aanvraag. Wij zullen die zo spoedig mogelijk behandelen.',
        'news-thx-msg' => 'Bedankt voor uw inschrijving. Wij houden u op de hoogte.',
        'mails' => [
            'contact' => [
                'title' => 'Beste, :first_name :last_name,',
                'message' => 'Bedankt voor uw aanvraag. Wij zullen die zo spoedig mogelijk behandelen.'
            ]
        ],
    ]
];
