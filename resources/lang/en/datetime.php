<?php

return [

    'day_of_week' => array (

        'short' => array (
            '01' => 'mon',
            '02' => 'tue',
            '03' => 'wed',
            '04' => 'thu',
            '05' => 'fri',
            '06' => 'sat',
            '07' => 'sun',
        ),
        'long' => array (
            '01' => 'monday',
            '02' => 'tuesday',
            '03' => 'wednesday',
            '04' => 'thursday',
            '05' => 'friday',
            '06' => 'saturday',
            '07' => 'sunday',
        )

    ),

    'month' => array (

        'short' => array (
            '01' => 'jan',
            '02' => 'feb',
            '03' => 'mar',
            '04' => 'apr',
            '05' => 'may',
            '06' => 'jun',
            '07' => 'jul',
            '08' => 'aug',
            '09' => 'sep',
            '10' => 'oct',
            '11' => 'nov',
            '12' => 'dec'
        ),
        'long' => array (
            '01' => 'january',
            '02' => 'february',
            '03' => 'march',
            '04' => 'april',
            '05' => 'may',
            '06' => 'june',
            '07' => 'july',
            '08' => 'august',
            '09' => 'september',
            '10' => 'october',
            '11' => 'november',
            '12' => 'december'
        )

    )

];