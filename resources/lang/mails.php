<?php
return [
    'email' => 'E-mailadres',
    'last_name' => 'Familienaam',
    'first_name' => 'Voornaam',
    'message' => 'Bericht',
    'question' => 'Vraag',
    'language' => 'Taal',
    'company' => 'Bedrijf'
];
