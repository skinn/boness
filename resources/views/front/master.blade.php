<!doctype html>
<html lang="{{App::getLocale()}}">

@section('groups')
@show
{{ $ec->addGroup(trans('labels.groups.seo.group'), ['sub_title' => trans('labels.groups.seo.sub_title')]) }}

<head>
    @include($layout . '::front.meta.meta')
    @include('front.assets.favicon')
    @include('front.assets.gtm')
    @include('front.assets.styles')
    @section('css')
    @show
</head>
<body id="page-{{ isset($page['page_id']) ? $page['page_id'] : 'unknown' }}"
      class="page page-{{ isset($page['page']['slug']) ? $page['page']['slug'] : 'unknown' }} page-{{ isset($page['page_id']) ? $page['page_id'] : 'unknown' }} front {{ \Flyfish\Modules\Auth\Facades\AuthService::allow(array(1,2)) ? 'logged-in': '' }}"
      page-id="{{ isset($page['page_id']) ? $page['page_id'] : 'unknown' }}">
<!--[if lte IE 9]><p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a
    href="http://browsehappy.com/">upgrade your browser</a> to improve your experience and security.</p><![endif]-->
    @include('front.assets.gtm-noscript')


<div class="site-wrapper">

    <div class="c-site-width">

        @section('header')
            @include('front.partials.general.header')
        @show

        <main>
            @yield('content')
        </main>

        @section('footer')
            @include('front.partials.general.footer')
        @show

    </div>

</div>

@include($layout . '::front.cms.cms')

@include('front.assets.scripts')

@section('js')
    <script type="text/javascript" src="https://services.skinn.site/cdn/gdpr/banner.js"></script>
    <script type="text/javascript">
        if (typeof PolicyBanner != "undefined") {
            PolicyBanner.init({
                name : "klantnaam", /* name of the client or website, wich will be shown in the text */
                disclaimerUrl: "{{ page_url('disclaimer') }}", /* url of the disclaimer page in the website */
                privacyPolicyUrl: "{{ page_url('privacypolicy') }}", /* url of the privacy policy page in de website (optional) */
                language: '{{ $language }}', /* language code to specify the text (values: nl, fr, en) */
                theme: 'black', /* color theme for the banner (values: black, white) */
                delay: 2000 /* delay after which the banner will be displayed (value: milliseconds) */
            });
        }
    </script>
@show

@include($layout . '::front.cms.entityScripts')

</body>
</html>
