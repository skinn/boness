@extends('front.master')

@section('content')
    <div class="container">
        <h1>Default template</h1>

        <h2>{{ $page['title'] }}</h2>

        <div>
            {!! $page['content'] !!}
        </div>

        <div style="border: 1px solid #ccc; margin: 10px; padding: 20px;">
            <h3>dataSources</h3>
            <ul>
                @foreach($dcc->getCollectionSlugs() as $slug)
                    <li>$dcc->getCollections('{{ $slug }}') as $record - (result count: {{ $dcc->count($slug)  }})</li>
                @endforeach
            </ul>
        </div>

        <div style="border: 1px solid #ccc; margin: 10px; padding: 20px;">

            <h3>widgets</h3>
            <ul>
                @foreach($ec->getKeys() as $slug)
                    <li>$ec->get('{{ $slug }}')</li>
                @endforeach
            </ul>
        </div>
        <div style="border: 1px solid #ccc; margin: 10px; padding: 20px;">
            <h3>tokens</h3>
            <ul>
                @foreach($tc->getKeys() as $category => $tokens)
                    <li>
                        <h4>{{ $category }}</h4>
                        <ul>
                            @foreach($tokens as $token)
                                <li>$tc->get('{{ $category}}, {{ $token }})</li>
                            @endforeach
                        </ul>
                    </li>
                @endforeach
            </ul>
        </div>
    </div>
@stop
