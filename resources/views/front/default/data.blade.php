@extends('front.master')

@section('content')
    <h1>data template</h1>

    <h2>{{ $page['title'] }}</h2>

    <div class="container">
        @foreach($dcc->getCollectionSlugs() as $slug)
            <div style="border: 1px solid #ccc; margin: 10px; padding: 20px;">
                <h2>{{ $slug }}</h2>
                @foreach($dcc->getCollection($slug) as $record)

                    @foreach($record as $alias => $values)
                        @foreach($values as $field => $value)
                            @if( ! is_object($value) && ! is_array($value))
                                {{ $alias }}.{{ $field }}: {{  $value }}<br/>
                            @endif

                        @endforeach
                        <br/>
                    @endforeach
                    <br/><br/><br/>
                @endforeach
            </div>
            <div>{!!  $dcc->pager($slug)  !!} </div>
        @endforeach
    </div>
@stop
