<script>window.env = {env: '{{ $env }}'}</script>

<script src="{{ mix('js/common/vendors.js') }}"></script>

<script src="{{ mix('js/ui/CommonApp.js')}}"></script>

@if(\Flyfish\Modules\Auth\Facades\AuthService::allow(array(1,2)))
    <script src="{{ mix('js/ui/BackApp.js')}}"></script>
@endif

<script src="{{ mix('js/ui/FrontApp.js')}}"></script>
<script src="{{ mix('js/site/app.js') }}"></script>
