<link href="{{ mix('css/common.css') }}" rel="stylesheet">

@if(\Flyfish\Modules\Auth\Facades\AuthService::allow(array(1,2)))
    <link href="{{ mix('css/boness.css') }}" rel="stylesheet">
@endif

<link href="{{ mix('css/app.css') }}" rel="stylesheet">
