<?php
$filteredmenu = array();
foreach($menu as $node) {
    if(!in_array($node['slug'], $skip) && (count($only) == 0 || in_array($node['slug'], $only)) && $level <= $max_level)
        $filteredmenu[] = $node;
}
?>

@if(count($filteredmenu) > 0)
    @if($parentTag)
        <{{$parentTag}}>
    @endif
    @foreach($filteredmenu as $node)
        {{--[[{{ print_r($node) }}]]<br/>--}}
        @if(!in_array($node['slug'], $skip) && (count($only) == 0 || in_array($node['slug'], $only)) && $level <= $max_level)
            @if($childTag)
                <{{$childTag}} class="l{{$level}} {{ $pages->isActive($node['slug'],  $node['url']) ? 'active' : '' }}">
            @endif
            @if($node['dynamic'])
                @if(\Illuminate\Support\Facades\View::exists('front.menus.' . $view_folder . '.' . $node['slug']))
                    @include('front.menus.' . $view_folder . '.' . $node['slug'])
                @else
                    front.menus.{{$view_folder}}.{{  $node['slug'] }}
                @endif
            @else
                <a href="{{ $node['url']  }}" class="c-dib s-menu__link"> {{ $node['label'] }}</a>
            @endif

            @if(!$node['dynamic'] && $pages->hasChildren($node['slug']))
                @if($parentTag)
                    <{{$parentTag}} class="c-db s-menu {{ $asTree ? 'sub-mnu' : '' }}">
                @endif
                @include('front.partials.general.menu.menu-node', [
                    'max_level' => $max_level,
                    'skip' => $skip,
                    'only' => $only,
                    'menu' => $pages->order($order)->useTree($asTree)->getChildren($node['slug']),
                    'order' => $order,
                    'asTree' => $asTree,
                    'level' => $level+1,
                    'node' => $node,
                    'parentTag' => $parentTag,
                    'childTag' => $childTag,
                    'view_folder' => $view_folder,
                    'url' => $node['url'],
                    'node' => $node,
                    'tokens' => []
                    ])
                @if($parentTag)
                    </{{$parentTag}}>
                @endif
            @endif
            @if($childTag)
                </{{$childTag}}>
            @endif
        @endif
    @endforeach
    @if($parentTag)
        </{{$parentTag}}>
    @endif
@endif

