    @if($childTag)
        <{{$childTag}} class="l{{$level}} {{ $pages->isActive($node['slug'], $url) ? 'active' : '' }}">
    @endif
    <a href="{{ $url }}">{{ $dynamicLabel}}</a>
    @if($pages->hasChildren($node['slug']))
        @if($parentTag)
            <{{$parentTag}} class="{{ $asTree ? 'sub-mnu' : '' }}">
        @endif
        @include('front.partials.general.menu.menu-node', [
            'max_level' => $max_level,
            'skip' => $skip,
            'only' => $only,
            'menu' => $pages->order($order)->useTree($asTree)->getChildren($node['slug']),
            'order' => $order,
            'asTree' => $asTree,
            'level' => $level+1,
            'parentTag' => $parentTag,
            'childTag' => $childTag,
            'view_folder' => $view_folder,
            'url' => $url ,
            'node' => $node,
            'tokens' => $tokens
            ]
            )
        @if($parentTag)
            </{{$parentTag}}>
        @endif
    @endif
    @if($childTag)
        </{{$childTag}}>
    @endif
