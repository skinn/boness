@if($pages->hasChildren($start))
    @if($parentTag)
    <{{$parentTag}}>
    @endif
        @include('front.partials.general.mobile-menu.menu-node', [
            'max_level' => $max_level-1,
            'skip' => $skip,
            'only' => $only,
            'menu' => $pages->order($order)->useTree($asTree)->getChildren($start),
            'order' => $order,
            'asTree' => $asTree,
            'level' => 0,
            'node' => [],
            'parentTag' => $parentTag,
            'childTag' => $childTag,
            'view_folder' => $view_folder,
            'url' => '/nl',
            'tokens' => []
            ])

    @if($parentTag)
    </{{$parentTag}}>
    @endif
@endif
