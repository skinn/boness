<nav>
    @foreach($pages['tree'][0] as $pageId)
        <a href="{{ page_url($pages['pages'][$pageId]['slug']) }}">{{trans('labels.menu.' . $pages['pages'][$pageId]['slug'])}}</a>
    @endforeach
</nav>
