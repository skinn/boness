@foreach($menu as $node)
    {{--[[{{ print_r($node) }}]]<br/>--}}
    @if( ! in_array($node['slug'], $skip) && (count($only) == 0 || in_array($node['slug'], $only) )&& $level <= $max_level)
        @if($childTag)
        <{{$childTag}} class="l{{$level}} {{ $pages->isActive($node['slug'],  $node['url']) ? 'active' : '' }}">
        @endif
            @if($node['dynamic'])
                @if(\Illuminate\Support\Facades\View::exists('front.menus.' . $view_folder . '.' . $node['slug']))
                    @include('front.menus.' . $view_folder . '.' . $node['slug'])
                @else
                    front.menus.{{$view_folder}}.{{  $node['slug'] }}
                @endif
            @else
                <a href="{{ $node['url']  }}" class="c-dib s-menu__link"> {{ $node['label'] }}</a>
            @endif
        @if($childTag)
        </{{$childTag}}>
        @endif
    @endif
@endforeach
