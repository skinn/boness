    @if($childTag)
        <{{$childTag}} class="l{{$level}} {{ $pages->isActive($node['slug'], $url) ? 'active' : '' }}">
    @endif
    <a href="{{ $url }}">{{ $dynamicLabel}}</a>
    @if($childTag)
        </{{$childTag}}>
    @endif
