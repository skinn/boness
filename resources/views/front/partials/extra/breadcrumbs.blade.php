<ul>
    @foreach($breadcrumbs as $key => $crumb)
        <li>
            <a href='{{ $crumb['url'] }}' class='xib {{($key == $active) ? 'active' : ''}}'>
                {{ (isset($crumb['label'])) ? $crumb['label'] : trans('labels.menu.' . $key) }}
            </a>
        </li>
    @endforeach
</ul>
