@if($parentTag)
    <{{$parentTag}} class="{{ $asTree ? 'sub-mnu' : '' }}">
@endif
@foreach($dcc->getCollection('global-blog-overview') as $blog)
    @include('front.partials.general.menu.menu-node-dynamic', [
        'url' => $pages->getDynamicUrl([$dcc->get($blog,'blog', 'slug')], $node['url']),
        'max_level' => $max_level,
        'skip' => $skip,
        'only' => $only,
        'menu' => $pages->order($order)->useTree($asTree)->getChildren($node['slug']),
        'order' => $order,
        'asTree' => $asTree,
        'level' => $level+1,
        'parentTag' => $parentTag,
        'childTag' => $childTag,
        'node' => $node,
        'view_folder' => $view_folder,
        'dynamicLabel' => $dcc->get($blog,'blog', 'slug'),
    ])
@endforeach
@if($parentTag)
    </{{$parentTag}}>
@endif
