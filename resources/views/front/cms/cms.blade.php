@if(\Flyfish\Modules\Auth\Facades\AuthService::allow(array(1,2)))
    <div id="boness" class="admin" v-cloak>
        <div class="body-xcms">
            <div class="cms-launcher" :class="{ 'cms-launcher--visible' : !cmsIsActive }">
                {{--<main-menu-button></main-menu-button>--}}
                <div class="xib sc-button sc-button--circle sc-button--cms-launcher large-circle"
                     @click.prevent="routeGo({path:'/page'})">
                    <div class="sc-button__circle">
                        <div>CMS</div>
                    </div>
                </div>
            </div>

            <div transition="expand" v-show="overlay">
                <router-view></router-view>
            </div>

            <div class="main-menu-overlay" :class="{'main-menu-overlay--visible': menu}">
                <main-menu></main-menu>
            </div>

            <input type="hidden" id="_ec" value="{{ json_encode($ec->getUsed()) }}"/>
            <input type="hidden" id="_tc" value="{{ json_encode($tc->getUsed()) }}"/>
            <input type="hidden" id="_dcc" value="{{ json_encode($dcc->getUsed()) }}"/>
            <input type="hidden" id="_user" value="{{ $user->name }}"/>
            <input type="hidden" id="_fgco" value="{{ json_encode($fgc->getGroupOptions()) }}"/>
            <input type="hidden" id="_fgc" value="{{ json_encode($fgc->getGroups()) }}"/>
            <input type="hidden" id="_sort" value="{{ json_encode($dcc->getSort()) }}"/>

        </div>
    </div>
@endif
