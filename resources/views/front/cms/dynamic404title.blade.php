@if($dcc->count($collectionSlug) == 0)
    @include($layout . '::front.meta.pageTitle', ['site' => 'install', 'title' => trans('labels.labels.404')])
@endif
