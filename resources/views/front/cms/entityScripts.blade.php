@if(class_exists('ComponentScriptContainer'))
    @foreach(ComponentScriptContainer::getScripts() as $script)
        {!! $script !!}
    @endforeach
@endif

@foreach($ec->getScripts() as $script)
    {!! $script !!}
@endforeach
