@if(isset($title))
    <title>
        {{ replacePageTitle(strip_tags($ec->group(trans('labels.groups.seo.group'))->get('page-title', false, '')), $title )}}
        | {{ strip_tags($tc->group(trans('labels.groups.seo.group'))->get('general', 'site-name')) }}
    </title>
@else
    <title>
        {{ strip_tags($ec->group(trans('labels.groups.seo.group'))->get('page-title')) }}
        | {{ strip_tags($tc->group(trans('labels.groups.seo.group'))->get('general', 'site-name')) }}
    </title>
@endif