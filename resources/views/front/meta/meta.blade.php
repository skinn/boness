@section('page-title')
    @include($layout . '::front.meta.pageTitle')
@show

<meta charset="utf-8">
<meta http-equiv="x-ua-compatible" content="ie=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta id="token" name="token" value="{{ csrf_token() }}">
<meta id="language" name="language" value="{{App::getLocale()}}">

@if(\Flyfish\Modules\Auth\Facades\AuthService::allow(array(1,2)))<meta id="mode" name="mode" value="front">@endif

@section('page-meta')
    @include($layout . '::front.meta.pageMeta')
@show
