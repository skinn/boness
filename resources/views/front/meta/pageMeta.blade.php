@if(isset($tokens))
    {!! replaceMetatagTokens($ec->group(trans('labels.groups.seo.group'))->get('metatags'), $tokens, $language) !!}
@else
    {!! $ec->group(trans('labels.groups.seo.group'))->get('metatags') !!}
@endif
