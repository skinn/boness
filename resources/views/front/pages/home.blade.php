@extends('front.master')

@section('css')
    @if(!\Flyfish\Modules\Auth\Facades\AuthService::allow(array(1,2)))
        <link rel="stylesheet" href="{{ mix('css/boness.css') }}">
    @endif
    <style>
        .welcome {
            font-family: 'HelveticaNeueMedium', 'Helvetica Neue', 'Helvetica', 'Arial', sans-serif;
            letter-spacing: 1px;
            text-align: center;
            margin-top: 200px;
        }

        .welcome__title,
        .welcome__text,
        .welcome__btn {
            opacity: 0;
            animation: fadeIn 0.8s 1;
            animation-fill-mode: forwards;
        }

        .welcome__title {
            animation-delay: 0.2s;
        }

        .welcome__text {
            animation-delay: 0.3s;
            margin-bottom: 20px;
        }

        .welcome__btn {
            animation-delay: 1s;
            background: #000;
            color: #fff;
            padding: 10px 15px;
            border-radius: 30px;
        }

        @keyframes fadeIn {
            0% {
                opacity: 0;
                transform: translateY(30px);
            }
            100% {
                opacity: 1;
                transform: translateY(0);
            }
        }
    </style>
@endsection

@section('content')
    <div class="welcome">
        <h1 class="welcome__title">&#128076; Installation complete! &#128293;</h1>
        <p class="welcome__text">Let's start building.</p>
        @if(!\Flyfish\Modules\Auth\Facades\AuthService::allow(array(1,2)))
            <a class="c-dib sc-button sc-button--submit welcome__btn" href="/admin">Log in</a>
        @else
            <a class="c-dib sc-button sc-button--submit welcome__btn" href="/admin">Go to Admin</a>
        @endif
    </div>
@endsection
