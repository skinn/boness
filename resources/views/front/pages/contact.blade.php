@extends('front.master')

@section('groups')
    {{ $ec->addGroup(trans('labels.groups.contact_form.group'), ['sub_title' => trans('labels.groups.contact.sub_title')]) }}
    {{ $ec->addGroup(trans('labels.groups.newsletter_form.group'), ['sub_title' => trans('labels.groups.newsletter_form.sub_title')]) }}
@endsection

@section('breadcrumbs')

    @include('front.partials.extra.breadcrumbs', ['active' => 'contact','breadcrumbs' => [
        'home' => ['url' => page_url('home')],
        'contact' => ['url' => page_url('contact')]
    ]])
@stop

@section('css')
    <style>
        [v-cloak] {
            display: none;
        }
    </style>
@endsection
@section('content')
    {{ $ec->addGroup(trans('labels.groups.contact_form.group'), ['sub_title' => trans('labels.groups.contact_form.sub_title')]) }}

    <h1>{{ $ec->group(trans('labels.groups.seo.group'))->get('page-title') }}</h1>
    <br><br><br><br>


    Standaard contactformulier:<br>
    {!! $ec->group(trans('labels.groups.contact_form.group'))->get('contact-form') !!}
    <br><br><br><br>


    Standaard nieuwsbrief:<br>
    {!! $ec->group(trans('labels.groups.newsletter_form.group'))->get('newsletter-form') !!}
@endsection
