@extends('front.master')

@section('content')

    <h1> {{ trans('labels.labels.404') }} </h1>

    <a href="/">{{ trans('labels.labels.to-homepage') }}</a>

@endsection
