@extends('front.master')

@section('groups')
    {{ $ec->addGroup(trans('labels.groups.content.group'), ['sub_title' => trans('labels.groups.content.sub_title')]) }}
@endsection

{{--@section('breadcrumbs')
    @foreach($dcc->group(trans('labels.groups.content.group'))->getCollection('article') as $record)
        @include('front.partials.extra.breadcrumbs', ['active' => 'article-detail','breadcrumbs' => [
        'home' => ['url' => page_url('home')],
        'articles' => ['url' => page_url('articles')],
        'article-detail' => [
            'url' => page_url('article', $dcc->get($record, 'articles', 'slug')),
            'label' => $dcc->get($record, 'articles', 'title')
            ]
        ]])
    @endforeach
@stop--}}

@section('page-title')

    @include($layout . '::front.cms.dynamic404title', ['collectionSlug' => 'article'])

    @foreach($dcc->group(trans('labels.groups.content.group'))->getCollection('article') as $record)
        @include($layout . '::front.meta.pageTitle', ['site' => 'install', 'title' => $dcc->get($record, 'articles', 'title')])
    @endforeach
@stop

@section('page-meta')

    @if($record = $dcc->group(trans('labels.groups.content.group'))->getFirstFromCollection('article'))
        @include($layout . '::front.meta.pageMeta',['tokens' => [
            'description' => $dcc->get($record, 'articles', 'description'),
            'twitter:card' => $dcc->get($record, 'articles', 'description'),
            'og:title' => $dcc->get($record, 'articles', 'title'),
            'og:type' => 'article',
            'og:description' => $dcc->get($record, 'articles', 'description'),
            'og:image' => $dcc->getAssetUrl($record, 'articles', 'media', ["w" => getConfig('ogImageWidth', 1200)])
        ]])
    @endif
@stop

@section('content')

    @include($layout . '::front.cms.dynamic404', ['collectionSlug' => 'article'])

    <div class="grid">

        @foreach($dcc->group(trans('labels.groups.content.group'))->getCollection('article') as $record)
            <div class="row">
                <div class="col col--full">
                    <div class="c-db responsive-hero">
                        <div class="c-abf responsive-hero__cover">
                            <img src="{{ $dcc->getAssetUrl($record, 'articles', 'image', ['w' => 60, 'q' => '80', 'format' => 'pjpg', 'fit' => 'crop', 'blur' => 10]) }}"
                                 class="c-db" />
                            <img media="orientation: landscape"
                                 data-srcset="{{
                                    $dcc->getAssetUrl($record, 'articles', 'image', ["q" => '80', "w" => 600]) . ' 600w, ' .
                                    $dcc->getAssetUrl($record, 'articles', 'image', ["q" => '80', "w" => 1024]) . ' 1024w, ' .
                                    $dcc->getAssetUrl($record, 'articles', 'image', ["q" => '80', "w" => 1400]) . ' 1400w, ' .
                                    $dcc->getAssetUrl($record, 'articles', 'image', ["q" => '80', "w" => 2400]) . ' 2400w, ' .
                                    $dcc->getAssetUrl($record, 'articles', 'image', ["q" => '80', "w" => 3000]) . ' 3000w'
                                 }}"
                                 data-sizes="{{
                                    '(max-width: 450px) 600px, ' .
                                    '(min-width: 451px) and (max-width: 768px) 800px, ' .
                                    '(min-width: 769px) and (max-width: 1024px) 1024px, ' .
                                    '(min-width: 1025px) 100vw'
                                 }}"
                                 alt=""
                                 class="c-abf lazyload blur-up"/>
                        </div>
                        <div class="c-ab responsive-hero__title">
                            <h1 class="c-db">{{ $dcc->get($record, 'articles', 'title') }}</h1>
                            <div class="c-db c-text c-text--white c-text--centered s-spacer-bottom--small">
                                {{ transDateDayLong($dcc->get($record, 'articles', 'date')) }}
                                {{ formatDate($dcc->get($record, 'articles', 'date'), 'd') }}
                                {{ transDateMonthLong($dcc->get($record, 'articles', 'date')) }}
                                {{ formatDate($dcc->get($record, 'articles', 'date'), 'Y') }}
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="row c-tc s-spacer-bottom--large s-spacer-top--large">
                <div class="col col--full c-text c-text--centered">
                    {!! $dcc->get($record, 'articles', 'intro') !!}
                </div>
            </div>


            <div class="row s-spacer-bottom--large">
                <div class="col col--full">
                    {!! $dcc->renderCollection(
                        'datalist-items',
                        $dcc->exclude()->getCollection('article-content', ['article_id' => $dcc->get($record,'articles','id')]),
                        'article_content',
                        'widgets.datalist.default.default')
                    !!}
                </div>
            </div>

            <div class="row row--wide row--between s-spacer-bottom--large">
                <div class="col col--4 c-tl">
                    @if(count($dcc->group('exclude')->getFirst('newer-article', ['article_date' => $dcc->get($record,'articles','date'), 'article_id' => $dcc->get($record,'articles','id')])))
                        <a href="{{ page_url('article-detail', null, $dcc->get($dcc->getFirst('newer-article', ['article_date' => $dcc->get($record,'articles','date'), 'article_id' => $dcc->get($record,'articles','id')]), 'newer_article', 'slug'),['type'])}}">
                            nieuwer : {{ $dcc->get($dcc->getFirst('newer-article', ['article_date' => $dcc->get($record,'articles','date'), 'article_id' => $dcc->get($record,'articles','id')]), 'newer_article', 'title') }}
                        </a>
                    @endif
                </div>
                <div class="col col--4 c-tc">
                    <a href="{{ page_url('articles') }}">{{ trans('pagination.back_to_overview') }}</a>
                </div>
                <div class="col col--4 c-tr">
                    @if(count($dcc->group('exclude')->getFirst('older-article', ['article_date' => $dcc->get($record,'articles','date'), 'article_id' => $dcc->get($record,'articles','id')])))
                        <a href="{{ page_url('article-detail', null, $dcc->get($dcc->getFirst('older-article', ['article_date' => $dcc->get($record,'articles','date'), 'article_id' => $dcc->get($record,'article','id')]), 'older_article', 'slug'),['type'])}}">
                            ouder : {{ $dcc->get($dcc->getFirst('older-article', ['article_date' => $dcc->get($record,'articles','date'), 'article_id' => $dcc->get($record,'articles','id')]), 'older_article', 'title') }}
                        </a>
                    @endif
                </div>
            </div>
        @endforeach
    </div>

@endsection
