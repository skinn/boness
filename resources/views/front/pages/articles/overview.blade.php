@extends('front.master')

@section('groups')
    {{ $ec->addGroup(trans('labels.groups.article.group'), ['sub_title' => trans('labels.groups.content.sub_title')]) }}
@endsection


{{--@section('breadcrumbs')

        @include('front.partials.extra.breadcrumbs', ['active' => 'articles','breadcrumbs' => [
            'home' => ['url' => page_url('home')],
            'articles' => ['url' => page_url('articles')]
        ]])
@stop--}}

@section('content')
    {{ $ec->addGroup(trans('labels.groups.article.group'), ['sub_title' => trans('labels.groups.article.sub_title')]) }}

    <h1 class="c-tc">{{ $ec->group(trans('labels.groups.seo.group'))->get('page-title') }}</h1>

    <div class="c-db s-grid s-grid-default">
        <div class="row">
            @foreach($dcc->group(trans('labels.groups.articles.group'))->getCollection('articles') as $record)
                <div class="col col--6-md col--4-lg">
                    <a href="{{ page_url('article-detail', null, $dcc->get($record, 'articles', 'slug')) }}" class="c-db s-grid__content">
                        <div class="c-db s-grid-image"><img src="{!! $dcc->getAssetUrl($record, 'articles', 'image', ["w" => '400']) !!}" class="c-db c-img" style="height: 100%; width: 100%;"/></div>
                        <h3 class="c-db s-grid-caption-title">{{ $dcc->get($record, 'articles', 'title') }}</h3>
                        <p class="c-db s-grid-caption">
                            <div>
                                {{ transDateDayLong($dcc->get($record, 'articles', 'date')) }}
                                {{ formatDate($dcc->get($record, 'articles', 'date'), 'd') }}
                                {{ transDateMonthLong($dcc->get($record, 'articles', 'date')) }}
                                {{ formatDate($dcc->get($record, 'articles', 'date'), 'Y') }}
                            </div>
                            <br>
                            {!! $dcc->get($record, 'articles', 'intro') !!}
                        </p>
                        <button class="c-dib s-grid-button">Read more</button>
                    </a>
                </div>

            @endforeach
        </div>
    </div>

    @foreach($dcc->getCollection('filter', ['min_rooms' => 1]) as $record)
        {{ var_dump($record) }}
    @endforeach

@endsection
