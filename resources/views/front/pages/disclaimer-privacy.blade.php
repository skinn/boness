@extends('front.master')

@section('groups')
    {{ $ec->addGroup(trans('labels.groups.content.group'), ['sub_title' => trans('labels.groups.content.sub_title')]) }}
@endsection

@section('content')
    {!! $ec->group(trans('labels.groups.content.group'))->get('content') !!}
@endsection
