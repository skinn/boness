@extends($layout . '::app')

@section('content')
    <div class="login">

        <a href="https://www.skinn.be" target="_blank" class="form__logo" rel="noopener noreferrer">
            <img class="form__logo" src="https://www.skinn.be/boness/logo_backoffice.svg">
        </a>

        <div class="form" id="login-form">
            <form autocomplete="off" role="form" method="POST" action="{{ url('/auth/login') }}">
                <input type="hidden" name="_token" value="{{ csrf_token() }}">

                <div class="form__field">
                    <input type="email" class="form__field-input" name="email" id="email" value="{{ old('email') }}">
                    <label for="email" class="form__field-label" id="lblEmail">Login</label>
                </div>

                <div class="form__field">
                    <input type="password" class="form__field-input" id="password" name="password">
                    <label for="password" class="form__field-label" id="lblPassword">Wachtwoord</label>
                </div>

                <div class="form__field c-tc">
                    <button type="submit" class="c-dib sc-button sc-button--submit">Inloggen</button>
                </div>

                @if (count($errors) > 0)
                    <ul class="form__validation-list" transition="validation-list">
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                @endif
            </form>
        </div>

    </div>
@endsection

@section ('js')
    <script>
        var $input = $('.form__field-input');

        $input.on('keydown', function () {
            checkInputClass(this)
        })

        // Check on browser autofill
        $input.on('change', function () {
            checkInputClass(this)
        })

        $input.on('blur', function () {
            checkInputClass(this)
        })

        function checkInputClass(input) {
            if (input.value !== '') {
                $(input).addClass('form__field-input--active')
            } else {
                $(input).removeClass('form__field-input--active')
            }
        }

        for (var i = 0; i < $input.length; i++) {
            checkInputClass($input[i])
        }

        let checkAutoFillInterval = setInterval(function() {
            passwordFieldHack()
        }, 100)

        function passwordFieldHack() {
            if ($("#email").val() !== '') {
                $("#lblEmail").addClass('form__field-label--fixed')
                $("#lblPassword").addClass('form__field-label--fixed')
                clearInterval(checkAutoFillInterval)
            }
        }

    </script>
@endsection
