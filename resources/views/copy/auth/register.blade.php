@extends($layout . '::back.master')

@section('content')

    <div id="register" class="back-container">
        <header class="back-header">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-md-12">
                        <h1>Registreer een gebruiker</h1>

                    </div>


                </div>
            </div>
        </header>
        <section class="back-content">
            <div class="container-fluid">
                <div class="row">

                    <div>
                        @if (count($errors) > 0)
                            <div class="row">
                                <div class="col-md-6" v-if="errors">

                                    <div class="alert alert-danger">
                                        <strong>Whoops!</strong> There were some problems with your input.<br><br>
                                        <ul>
                                            @foreach ($errors->all() as $error)
                                                <li>{{ $error }}</li>
                                            @endforeach
                                        </ul>
                                    </div>

                                </div>
                            </div>
                        @endif
                        <div class="row">
                            <div class="col-md-6">
                                <form class="form-horizontal" role="form" method="POST"
                                      action="{{ url('/auth/register') }}">

                                    <input type="hidden" name="_token" value="{{ csrf_token() }}">

                                    <div class="form-group">
                                        <label class="control-label">Name</label>
                                        <input type="text" class="form-control" name="name" value="{{ old('name') }}">
                                    </div>

                                    <div class="form-group">
                                        <label class="control-label">E-Mail Address</label>
                                        <input type="email" class="form-control" name="email"
                                               value="{{ old('email') }}">
                                    </div>

                                    <div class="form-group">
                                        <label class="control-label">Password</label>

                                        <input type="password" class="form-control" name="password">

                                    </div>

                                    <div class="form-group">
                                        <label class="control-label">Confirm Password</label>

                                        <input type="password" class="form-control" name="password_confirmation">

                                    </div>

                                    <div class="form-group">

                                        <button type="submit" class="btn btn-primary">
                                            Register
                                        </button>

                                    </div>
                                </form>
                            </div>
                        </div>

                    </div>

                </div>
        </section>
        <footer class="back-footer">

        </footer>
    </div>
@endsection
