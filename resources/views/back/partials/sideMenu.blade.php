<aside class="bo-sideBar">
    <div class="sideBar__logoContainer">
        <a href="{{ asset('/') }}" target="_blank" class="sideBar__logo">
            <img src="https://www.skinn.be/boness/logo_backoffice.svg" />
        </a>
        <span class="sideBar__tagline">Backoffice</span>
    </div>
    <nav class="sideBar__navigation">
        <ul class="mainMenu">
            <li class="item mainMenu__item">

                <a class="mainMenu__link" v-link="{ path: '/home' }"><i class="fa fa-fw fa-home"></i> Dashboard <span v-bind="route"></span></a>
            </li>
            <li class="item mainMenu__item">
                <a class="mainMenu__link" href="#" v-link="{ path:'/admin/#panel.data_overview', activeClass: 'v-parentlink-active' }"><i class="fa fa-fw fa-database"></i>Inhoud</a>

                <ul class="subMenu">
                    <li class="item subMenu__item">
                        <a class="subMenu__link" v-link="{ path: '/admin/#panel.datasources_overview' }">Data Sources</a>
                    </li>
                    <li class="item subMenu__item">
                        <a class="subMenu__link" v-link=" {path:'/admin/#panel.data_overview' }">Data Content</a>
                    </li>
                    <li class="item subMenu__item">
                        <a class="subMenu__link" v-link="{ path: '/admin/#panel.datacollections_overview' }">Data Collections</a>
                    </li>
                    <li class="item subMenu__item">
                        <a class="subMenu__link" v-link="{ path: '/admin/#panel.tokens_overview' }">Tokens</a>
                    </li>
                    <li class="item subMenu__item">
                        <a class="subMenu__link" v-link="{ path: '/admin/#panel.assets_overview' }">Bestanden</a>
                    </li>
                </ul>
            </li>
            <li class="item mainMenu__item">
                <a class="mainMenu__link" v-link="{ path: '/admin/#panel.pages_tree' }"><i class="fa fa-fw fa-file-text-o"></i>Pagina's</a>
            </li>
            <li class="item mainMenu__item">
                <a class="mainMenu__link" v-link="{ path: '/admin/#panel.settings_overview'} "><i class="fa fa-fw fa-cogs"></i>Instellingen</a>
            </li>
            <li class="item mainMenu__item">
                <a class="mainMenu__link" v-link="{ path: '/admin/#panel.users_overview'}"><i class="fa fa-fw fa-users"></i>Gebruikers</a>
            </li>
            <li class="item mainMenu__item">
                <a class="mainMenu__link" href="{{ url('/auth/logout') }}"><i class="fa fa-fw fa-sign-out"></i>Afmelden</a>
            </li>
        </ul>
    </nav>
</aside>
