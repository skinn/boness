<header>
    <nav class="navbar navbar-custom">
        <div class="container-fluid">
            <div>
                <ul class="nav navbar-nav">
                    <li><a v-link="{ name: 'root' }">Dashboard</a></li>
                    <li><a v-link="{ name: 'pages' }">Pages</a></li>
                    <li><a v-link="{ name: 'dataCollections' }">Data collections</a></li>
                    <li><a v-link="{ name: 'dataSources' }">Data sources</a></li>
                    <li><a v-link="{ name: 'data' }">Data</a></li>
                    <li><a v-link="{ name: 'assets' }">Assets</a></li>
                    <li><a v-link="{ name: 'tokens' }">Tokens</a></li>
                </ul>
            </div>
        </div>
    </nav>
</header>