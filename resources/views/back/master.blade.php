<!doctype html>
<html lang="{{App::getLocale()}}" class="admin">
<head>
    @section('meta')
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta id="token" name="token" value="{{ csrf_token() }}">
    <meta id="language" name="language" value="{{App::getLocale()}}">
    <meta id="mode" name="mode" value="back">
    @show

    <title>skinnCMS</title>

    @include($layout . '::back.styles')
    @section('css')
    @show
</head>
<body id="boness">
    <div class="body-xcms backoffice">
        @include($layout . '::back.partials.sideMenu')

        <main class="bo-main">
            @yield('content')

            <div class="overlay" :class="{'show': overlay}" v-on:click="overlayAction('root')"></div>
        </main>
    </div>

    <toaster></toaster>

    @include($layout . '::back.scripts')
    @section('js')
    @show

</body>
</html>
