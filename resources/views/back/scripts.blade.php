<script>window.env = { env: '{{ $env }}' }</script>
<script src="{{ mix('js/common/vendors.js') }}"></script>
<script src="{{ mix('js/boness/vendors.js') }}"></script>
<script src="{{ mix('js/ui/CommonApp.js')}}"></script>
<script src="{{ mix('js/ui/BackApp.js') }}"></script>
<script src="{{ mix('js/boness/app.js') }}"></script>
