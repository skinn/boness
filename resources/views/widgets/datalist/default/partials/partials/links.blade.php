@if($links)
    <ul class="dli__links">
        @foreach(json_decode($links, true) as $link)
            <li class="dli__link">
                <a class="link" href="{{$link['url']}}">{{$link['label']}}</a>
            </li>
        @endforeach
    </ul>
@endif

