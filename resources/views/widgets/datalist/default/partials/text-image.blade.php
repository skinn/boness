<div class="c-container">

    <div class="c-db s-datalist__item">
        <div class="row {{ $textimageindex % 2 == 0 ? 'row--odd' : 'row--even' }}">
            <div class="col col--full col--6-md s-spacer-bottom--medium">
                <img srcset="{{
                        $dcc->getAssetUrl($record, $table, 'image', ['w' => 700, 'q' => 80]) . ' 700w, ' .
                        $dcc->getAssetUrl($record, $table, 'image', ['w' => 1400, 'q' => 80]) . ' 1400w, ' .
                        $dcc->getAssetUrl($record, $table, 'image', ['w' => 2400, 'q' => 80]) . ' 2400w'
                     }}"
                     sizes="(max-width: 750px) 100vw, (min-width: 751px) 45vw"
                     alt="{{ strip_tags($dcc->get($record, $table, 'title')) }}"
                     class=""/>
            </div>
            <div class="col col--full col--6-md">
                @if($dcc->get($record, $table, 'title') != '')
                    <div class="c-title c-title--medium s-spacer-bottom--medium">
                        {{ $dcc->get($record, $table, 'title') }}
                    </div>
                @endif
                <div class="c-text s-spacer-bottom--medium">
                    {!! $dcc->get($record, $table, 'text') !!}
                </div>
            </div>
        </div>
    </div>

</div>
