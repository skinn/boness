<div class="c-container">

    @foreach($dcc->group(trans('labels.groups.seo.group'))->getAssetsUrl($record, $table, 'image') as $image)
        <div class="c-db">
            <img srcset="{{
                    adjustImage($image, ['w' => 700, 'q' => '80']) . ' 700w, ' .
                    adjustImage($image, ['w' => 1200, 'q' => '80']) . ' 1200w, ' .
                    adjustImage($image, ['w' => 2400, 'q' => '80']) . ' 2400w, '
                 }}"
                 sizes="(max-width: 1200px) 100vw, (min-width: 1200px) 1200px"
                 alt=""
                 class=""/>
        </div>
    @endforeach

</div>
