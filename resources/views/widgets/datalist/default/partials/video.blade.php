<div class="c-container">
    @if(strip_tags(trim($dcc->get($record, $table, 'title'))) != '')
        <h3 class="c-title c-title--medium c-tc s-spacer-bottom--medium">{{ strip_tags($dcc->get($record, $table, 'title')) }}</h3>
    @endif

    {!! getVideo($dcc->get($record, $table, 'video'), $dcc->getAssetUrl($record, $table, 'image', ['w' => 1020, 'h' => 574, 'fit' => 'crop', 'q' => 80])) !!}
</div>
