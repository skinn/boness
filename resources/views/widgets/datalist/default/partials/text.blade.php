<div class="c-container">

    <div class="c-db s-datalist__item">
        <div class="row">
            <div class="col col--full">
                <div class="c-db">
                    @if($dcc->get($record, $table, 'title') != '')
                        <div class="c-title c-title--medium c-tc s-spacer-bottom--medium">
                            {{ $dcc->get($record, $table, 'title') }}
                        </div>
                    @endif
                    <div class="c-text c-text--centered s-spacer-bottom--medium">
                        {!! $dcc->get($record, $table, 'text') !!}
                    </div>
                </div>
            </div>
        </div>
    </div>

</div>
