<?php $textimageindex = 0; ?>

@if(count($datalistItems))
    @foreach($datalistItems as $index => $record)

        <div class="s-spacer-bottom--large">

            @if($dcc->get($record, $table, 'type') == 'text-image')
                @include('widgets.datalist.default.partials.text-image', ['textimageindex' => $textimageindex])
                <?php $textimageindex++; ?>
            @elseif($dcc->get($record, $table, 'type') == 'text')
                @include('widgets.datalist.default.partials.text')
            @elseif($dcc->get($record, $table, 'type') == 'image')
                @include('widgets.datalist.default.partials.image')
            @elseif($dcc->get($record, $table, 'type') == 'video')
                @include('widgets.datalist.default.partials.video')
            @endif

        </div>

    @endforeach
@endif
