<div id='{{$elementId}}' class='xb contact_form'>
    <div v-show='!showMessage'>
        <div>
            <div>
                <input type="text" v-model="model.email" placeholder="{{ trans('labels.text.email') }}">
            </div>
            <div v-if="hasError('email')" v-cloak>
                @{{ errors.email }}
            </div>
        </div>
        <button v-on:click='onSubmitForm'>{{ trans('labels.button.send') }}</button>
    </div>
    <div v-if='showMessage'>
        {{ trans('labels.text.newsletter-thx-msg') }}
    </div>
</div>