<div id='{{$elementId}}' class='xb contact_form'>
    <div v-show='!showMessage'>
        <div>
            <div>
                <input type="text" v-model="model.first_name" placeholder="{{ trans('labels.text.first-name') }}">
            </div>
            <div v-if="hasError('first_name')" v-cloak>
                @{{ errors.first_name }}
            </div>
        </div>

        <div>
            <div>
                <input type="text" v-model="model.last_name" placeholder="{{ trans('labels.text.last-name') }}">
            </div>
            <div v-if="hasError('last_name')" v-cloak>
                @{{ errors.last_name }}
            </div>
        </div>

        <div>
            <div>
                <input type="text" v-model="model.email" placeholder="{{ trans('labels.text.email') }}">
            </div>
            <div v-if="hasError('email')" v-cloak>
                @{{ errors.email }}
            </div>
        </div>
        <div>
            <div>
                <textarea  v-model="model.message" placeholder="{{ trans('labels.text.message') }}"></textarea>
            </div>
            <div v-if="hasError('message')" v-cloak>
                @{{ errors.message }}
            </div>
        </div>

        <button v-on:click='onSubmitForm'>{{ trans('labels.button.send') }}</button>
    </div>
    <div v-if='showMessage'>
        {{ trans('labels.text.contact-thx-msg') }}
    </div>

</div>
