# Skinn/Frontend Boilerplate

This is a boilerplate intended for internal projects.
It serves as a starting point for your front-end development.

## Installation
1. Run `npm install` if you haven't already.
2. That's it.

## Getting started

All code is run through [Gulp](http://gulpjs.com/) for an automated workflow.
The project folder should contain a Gulpfile in which your assets will be watched and compiled using [Laravel Elixir 5.0](https://laravel.com/docs/5.2/elixir).

The following **commands** are available in your terminal and should be executed from your root project folder:

* `gulp` : builds and compiles your Sass and JavaScript files **for Development**.
* `gulp --production` : builds and compiles your Sass and JavaScript files **for Production**. Also minifies your CSS and JS.
* `gulp watch` : watches your Sass and JavaScript files for any changes and compiles them automatically.
* `gulp clean` : removes your compiled files.

## Sass

All code that is exclusive to the project will be found in the `assets/sass/site` folder. **This is the folder you should be working in.**

Other folders should be not be edited as they are important to the styling of the CMS.

The Sass structure is based on the [sass-boilerplate](https://github.com/HugoGiraudel/sass-boilerplate) by Hugo Giraudel. It uses the [7-1 architecture pattern](http://sass-guidelin.es/#architecture) and sticks to [Sass Guidelines](http://sass-guidelin.es) writing conventions.

### Autoprefixer

All newer CSS that still need prefixes are generated automatically by [autoprefixer](https://www.npmjs.com/package/gulp-autoprefixer) through Laravel Elixir.

## JavaScript

The project has one entrypoint for writing JavaScript. The files are compiled using Browserify via Laravel Elixir. This allows requiring modules in the browser and using ECMAScript 6 and JSX.

Every file is also checked through [ESLint](http://eslint.org/) using the Babel parser.

New files will be compiled to usable JavaScript and are put in the `public` folder.

## Images

Any images that are loaded via CSS will be run through a lossless compression algorithm for optimal file size.

## Fonts + Icons

These assets are automatically copied to the `public` folder.

## BrowserSync

When watching files with Gulp, changes will automatically be injected to your browser via [BrowserSync](https://browsersync.io).

**Important: `gulp watch` can only be called from your host machine, it will not work from within Vagrant.**.
