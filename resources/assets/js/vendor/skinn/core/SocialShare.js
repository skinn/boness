import {getWindowWidth, getWindowHeight, isMobileDevice} from 'vendor/skinn/core/Helpers';


export default {

    _openPopupCenter: function(pageURL, title, w = 800, h = 600) {
        let left = (getWindowWidth() - w) / 2;
        let top = (getWindowHeight() - h) / 3;  // for 25% - devide by 4  |  for 33% - devide by 3
        let targetWin = window.open(pageURL, title, 'toolbar=no, location=no, directories=no, status=no, menubar=no, scrollbars=no, resizable=no, copyhistory=no, width=' + w + ', height=' + h + ', top=' + top + ', left=' + left);
    },

    /* <a href="{{ urlencode(request()->fullUrl()) }}"></a> */
    facebook(target) {
        let that = this;
        target.addEventListener('click', (e) => {
            let sharer = "https://www.facebook.com/sharer.php?u=";
            that._openPopupCenter(sharer + target.getAttribute("href"), "share on Facebook");
            e.preventDefault();
        });
    },

    /* <a href="{{ urlencode(request()->fullUrl()) }}"></a> */
    twitter(target) {
        let that = this;
        target.addEventListener('click', (e) => {
            let sharer = "https://twitter.com/intent/tweet?url=";
            that._openPopupCenter(sharer + target.getAttribute("href"), "share on Twitter");
            e.preventDefault();
        });
    },

    /* <a href="whatsapp://send?text={{ urlencode(request()->fullUrl()) }}" data-action="share/whatsapp/share" data-web-href="{{ urlencode(request()->fullUrl()) }}"></a> */
    whatsapp(target) {
        let that = this;
        target.addEventListener('click', (e) => {
            if(!isMobileDevice()) {
                let sharer = "https://web.whatsapp.com/send?text=";
                that._openPopupCenter(sharer + target.getAttribute("data-web-href"), "share on WhatsApp");
                e.preventDefault();
            }
        });
    },

    /* <a href="{{ urlencode(request()->fullUrl()) }}"></a> */
    linkedin(target) {
        let that = this;
        target.addEventListener('click', (e) => {
            let sharer = "https://www.linkedin.com/shareArticle?mini=true&url=";
            that._openPopupCenter(sharer + target.getAttribute("href"), "share on LinkedIn");
            e.preventDefault();
        });
    },

    /* place following script on page: <script type="text/javascript" async defer src="//assets.pinterest.com/js/pinit.js"></script>
    * <a href="javascript:;"></a> */
    pinterest(target) {
        let that = this;
        target.addEventListener('click', (e) => {
            PinUtils.pinAny();
            e.preventDefault();
        });
    }
}
