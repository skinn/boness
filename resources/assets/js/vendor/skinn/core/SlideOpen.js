const EventEmitter = require('events');

/*
// README
// this plugin sets the neccesary css-classes on elements to create a slide-open / slide-close functionality
// You can supply multiple trigger buttons that open and close one target

// EXAMPLE

    let mobileMenu = new SlideOpen(["js-hamburger", "js-sticky-hamburger"], "js-mobile-menu");
    mobileMenu.addListener("open", (triggers) => {
        window.scrollTo(0,0);
        for (let trigger of triggers) {
            trigger.classList.toggle("is-active");
        }
    });
    mobileMenu.addListener("close", (triggers) => {
        for (let trigger of triggers) {
            trigger.classList.toggle("is-active");
        }
    });
*/


export default class SlideOpen extends EventEmitter {

    constructor(triggerElementIds, targetElementId, closed = true) {
        super();
        this.openClass = "is-open";
        this.activeClass = "is-active";
        this.triggers = [];

        for (let id of triggerElementIds) {
            this.triggers.push(document.getElementById(id));
        }

        this.target = document.getElementById(targetElementId);

        if(this.triggers && this.target) {
            this.init(closed);
        }
    }

    init(closed) {
        for (let trigger of this.triggers) {
            trigger.addEventListener('click', this.onTriggerClicked.bind(this));
            if(closed) {
                TweenMax.set(this.target, { height: 0, opacity: 0 });
            }
        }
    }

    onTriggerClicked(e) {
        e.preventDefault();
        this.target.classList.toggle(this.openClass);

        let timeline = new TimelineMax();

        if (!this.isOpen()) {
            this.emit("close", this.triggers);
            timeline
                .to(this.target, 0.4, { opacity: 0, ease: Power4.easeOut })
                .to(this.target, 0.4, { height: 0, ease: Power4.easeOut, delay: -0.3 });
        } else {
            this.emit("open", this.triggers);
            TweenMax.set(this.target, { height: 'auto', opacity: 1 });
            timeline
                .from(this.target, 0.4, { height: 0, ease: Power4.easeOut})
                .from(this.target, 0.4, { opacity: 0, ease: Power4.easeOut, delay: -0.3 });
        }
    }

    isOpen() {
        return this.target.classList.contains(this.openClass);
    }

}
