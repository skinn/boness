import Header from 'vendor/skinn/core/header/Header'

/*

// README:
// this plugin provides the animation for showing and hiding a header when scrolling down (or up) a page
// this plugin does not provide any CSS, except for the animation transforms from TweenMax

// EXAMPLE

    HTML:
    <div class="js-revealelement" style="position:fixed;top: O;">reveal</div>

    JS:
    new RevealHeader(document.querySelector(".js-revealelement"), {
        triggerPoint: 800, // can be number or DOM element (top of element is used)
        revealSpeed: 0.5, //speed of animation down
        revealEase: Power2.easeOut, //change default easing of animation down
        concealSpeed: 1, //speed of animation up (take into account rubber banding, so distance is double)
        concealEase: Power2.easeOut, //change default easing of animation up
        onlyRevealOnScrollUp: false, //sometimes, you only want the header to appear when you scroll back up the page
        onRevealStarted : function() {
            console.log("reveal started");
        },
        onRevealFinished : function() {
            console.log("reveal finished");
        },
        onConcealStarted : function() {
            console.log("conceal started");
        },
        onConcealFinished: function() {
            console.log("conceal finished");
        }
    });

 */


class RevealHeader extends Header {

    constructor(element, options = {}) {
        super(element, options);

        this.triggerPoint = options.triggerPoint || 800;
        this.revealSpeed = options.revealSpeed || 0.5;
        this.revealEase = options.revealEase || Power2.easeOut;
        this.concealSpeed = options.concealSpeed || 1;
        this.concealEase = options.concealEase || Power2.easeOut;
        this.onlyRevealOnScrollUp = options.onlyRevealOnScrollUp || false;

        this.onRevealStarted = options.onRevealStarted || function() {};
        this.onRevealFinished = options.onRevealFinished || function() {};
        this.onConcealStarted = options.onConcealStarted || function() {};
        this.onConcealFinished = options.onConcealFinished || function() {};

        this._revealWatcher = null;

        this._revealed = false;
        this._concealed = false;

        this._scrollUp = false;
        this._scrollDown = true;

        this._lastScrollTop = 0;
        this._lastScrollTopHeader = 0;
        this._startPosition = '-200%'; //prevent weirdness because of "rubber banding" in browsers on Mac

        this._intialise();
    }

    _intialise() {
        let self = this;

        TweenMax.set(self.element, { y: self._startPosition });

        self._revealWatcher = scrollMonitor.create(self.triggerPoint);
        self._revealWatcher.stateChange(function(e) {
            if (self._revealCheck() && !self._concealCheck()) {
                self._revealAnimation();
            }
            if (self._concealCheck()) {
                self._concealAnimation();
            }
        });

        if(this.onlyRevealOnScrollUp)
            self._detectScrollDirection();
    }

    _revealCheck() {
        let self = this;
        return (self.onlyRevealOnScrollUp && self._scrollUp) || (!self.onlyRevealOnScrollUp && self._revealWatcher.isAboveViewport);
    }

    _concealCheck() {
        let self = this;
        return (self.onlyRevealOnScrollUp && self._scrollDown) || (self._revealWatcher.isBelowViewport || self._revealWatcher.isFullyInViewport);
    }

    _revealAnimation() {
        let self = this;

        if(self._revealed)
            return;

        self._revealed = true;
        self._concealed = !self._revealed;

        self.onRevealStarted();
        TweenMax.to(self.element, self.revealSpeed, { y: '0%', ease: self.revealEase, onComplete: function() {
                self.onRevealFinished();
            }});
    }

    _concealAnimation() {
        let self = this;

        if(self._concealed)
            return;

        self._concealed = true;
        self._revealed = !self._concealed;

        self.onConcealStarted();
        let concealFinishedCalled = false;
        TweenMax.to(self.element, self.concealSpeed, { y: self._startPosition, ease: self.concealEase, onUpdate: function() {
                //because startposition is -200%, use onUpdate to check if element is outside of viewport
                if(self.element._gsTransform.yPercent < -100 && !concealFinishedCalled) {
                    self.onConcealFinished();
                    concealFinishedCalled = !concealFinishedCalled;
                }
            }});
    }

    _detectScrollDirection() {
        let self = this;

        // Detect request animation frame
        let requestAF = window.requestAnimationFrame ||
            window.webkitRequestAnimationFrame ||
            window.mozRequestAnimationFrame ||
            window.msRequestAnimationFrame ||
            window.oRequestAnimationFrame ||
            // IE Fallback, you can even fallback to onscroll
            function(callback){ window.setTimeout(callback, 1000/60) };

        function loop(){
            // Avoid calculations if not needed
            if (self._lastScrollTop === window.pageYOffset) {
                requestAF(loop);
                return false;
            } else self._lastScrollTop = window.pageYOffset;

            self._updatePosition();

            // Recall the loop
            requestAF(loop)
        }

        // Call the loop for the first time
        loop();
    }

    _updatePosition () {
        let self = this;

        // Make sure they scroll more than delta
        if(Math.abs(self._lastScrollTopHeader - self._lastScrollTop) <= 100) //offset
            return;

        if (self._lastScrollTop > self._lastScrollTopHeader) {
            self._scrollDown = true;
            self._scrollUp = !self._scrollDown;
            self._concealAnimation();
        } else {
            self._scrollUp = true;
            self._scrollDown = !self._scrollUp;
            if(!self._concealCheck()) {
                self._revealAnimation();
            }
        }

        self._lastScrollTopHeader = self._lastScrollTop;
    }
}

export default RevealHeader;
