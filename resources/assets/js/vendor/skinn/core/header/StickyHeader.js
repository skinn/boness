import Header from 'vendor/skinn/core/header/Header'
import { getOffset } from 'vendor/skinn/core/Helpers';

/*
// README:
// this plugin changes the position of an element to "fixed" when the element leaves the viewport (css-class "is-fixed" is added)
// this plugin adds a "is-minimized" css-class when scrolling down a page. This can be used to change the appearence of the element
// this plugin does not set any CSS, except for setting the element "position:fixed"

// EXAMPLE
    new StickyHeader(document.querySelector(".js-stickyelement"), {
        shrinkOffset: 200 //calculated from where the header is fixed, set an offset when the element should get the class "is-minimized"
        onFixed: function() {
            console.log("element is fixed");
        },
        onUnfixed: function() {
            console.log("element is not fixed");
        },
        onMinimized: function() {
            console.log("element is minimized")
        },
        onMaximized: function() {
            console.log("element is maximized")
        }
    });

 */



class StickyHeader extends Header {

    constructor(element, options = {}) {
        super(element, options);

        this.shrinkOffset = options.shrinkOffset || 200;

        this.onFixed = options.onFixed || function() {};
        this.onUnfixed = options.onUnfixed || function() {};
        this.onMinimized = options.onMinimized || function() {};
        this.onMaximized = options.onMaximized || function() {};

        this._isAlwaysFixed = window.getComputedStyle(this.element).position === 'fixed';
        this.isFixed = this._isAlwaysFixed;
        this.isMinimized = false;

        this._initialise();
    }

    _initialise() {
        let self = this;

        if(self._isAlwaysFixed) {
            self.element.classList.add('is-fixed');
            self.onFixed();
        }

        let stickyWatcher = scrollMonitor.create(self.element);
        stickyWatcher.lock();
        stickyWatcher.stateChange(function() {
            if(!self._isAlwaysFixed) {
                if(!self.isFixed && this.isAboveViewport) {
                    self.isFixed = true;
                    self.element.classList.add('is-fixed');
                    self.element.style.setProperty('position', 'fixed');
                    self.onFixed();
                } else if (self.isFixed && !this.isAboveViewport) {
                    self.isFixed = false;
                    self.element.classList.remove('is-fixed');
                    self.element.style.removeProperty('position');
                    self.onUnfixed();
                }
            }
        });

        let headerPosition = self._isAlwaysFixed ? 0 : getOffset(self.element).top;
        let shrinkTrigger = headerPosition + self.shrinkOffset;

        let shrinkWatcher = scrollMonitor.create(shrinkTrigger);
        shrinkWatcher.stateChange(function() {
            if(!self.isMinimized && this.isAboveViewport) {
                self.isMinimized = true;
                self.element.classList.add('is-minimized');
                self.onMinimized();
            } else if (self.isMinimized && !this.isAboveViewport) {
                self.isMinimized = false;
                self.element.classList.remove('is-minimized');
                self.onMaximized();
            }
        });
        self.onMaximized();
    }
}

export default StickyHeader;
