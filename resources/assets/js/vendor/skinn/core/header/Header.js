import scrollMonitor from "scrollmonitor";
import { getOffset } from 'vendor/skinn/core/Helpers';

class Header {

    constructor(element, options = {}) {
        if(!element) {
            console.error('DOM element for header not found');
            return;
        }

        this.element = element;
        this.options = options;
    }

}

export default Header;
