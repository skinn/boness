import { forEach, getOffset } from 'vendor/skinn/core/Helpers'
import scrollMonitor from "scrollmonitor";

/*
// README
// This plugin enables developers to create scroll-to functionality, for instance on a onepager.
// Simply initialise the plugin by supplying the trigger-links (for example: menu buttons).
//

// EXAMPLE

    new ScrollTo({
        hyperlinks: document.querySelectorAll(".js-navigation a, .js-mobile-navigation a"),
        offset: getWindowWidth() > 768 ? -100 : -30,
        updateUrl: false
    });

 */

export default class ScrollTo {

    constructor(args = {}) {
        this.hyperlinks = args.hyperlinks || null;
        this.offset = typeof args.offset !== 'undefined' ? args.offset : 0;
        this.updateUrl = typeof args.updateUrl !== 'undefined' ? args.updateUrl : true;
        this.delay = typeof args.delay !== 'undefined' ? args.delay : 0;
        this.speed = typeof args.speed !== 'undefined' ? args.speed : 0.8;

        this.loggedIn = document.querySelector('body').classList.contains('logged-in');
        this.scrollInProgress = false;

        this.targets = [];

        this._initialise();
    }

    _initialise() {
        let self = this;

        // Add listeners
        if(self.hyperlinks !== null) {
            forEach(self.hyperlinks, (i, hyperlink) => {
                let href = hyperlink.getAttribute('href');
                if (href && href !== "") {
                    self.addTargetToCollection(href);
                    hyperlink.addEventListener('click', (e) => { self.clickLink(e) })
                }
            });

            forEach(self.targets, (i, target) => {
                self.setActiveOnScroll(target);
            });
        }
    }

    clickLink(e) {
        let self = this;

        let href = e.currentTarget.getAttribute('href');
        if (href && href !== "") {
            e.preventDefault();
            let target = document.querySelector(href);
            self.scroll(target, this.offset, this.updateUrl, this.delay, this.speed);
            self.setActiveLinks(href);
        }
    }

    addTargetToCollection(target) {
        let self = this;
        if(!self.targets.includes(target)) {
            self.targets.push(target);
        }
    }

    setActiveOnScroll(target) {
        let self = this;

        let linkWatcher = scrollMonitor.create(target);
        linkWatcher.enterViewport(function() {
            self.setActiveLinks(target);
        });
    }

    setActiveLinks(target) {
        let self = this;

        if(self.scrollInProgress)
            return;

        forEach(self.hyperlinks, (i, hyperlink) => {
            hyperlink.classList.remove("is-active");
        });
        let targetLinks = document.querySelectorAll("a[href^='" + target + "'");
        forEach(targetLinks, (i, hyperlink) => {
            hyperlink.classList.add("is-active");
        });
    }

    scroll(targetElement, offset = 0, updateUrl = true, delay = 0, speed = 0.8) {
        let self = this;

        if (!targetElement || self.scrollInProgress)
            return false;

        let topY = getOffset(targetElement).top + offset;

        // Don't scroll to when too close to destination
        if (topY - window.pageYOffset < 100 && topY - window.pageYOffset > -100)
            return false;

        TweenMax.to(window, speed, {
            scrollTo: {
                y: topY,
                autoKill: false
            },
            ease: Power3.easeInOut,
            onStart: () => {
                self.scrollInProgress = true;
                // Change URL only when not logged in
                if (!this.loggedIn && updateUrl && targetElement.getAttribute("id"))
                    location.hash = targetElement.getAttribute("id");
            },
            onComplete: () => {
                self.scrollInProgress = false
            },
            delay: delay
        })
    }


}
