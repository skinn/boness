// -----------------------------------------------------------------------------
// Datalist
// -----------------------------------------------------------------------------

export const version = '1.0'

// Inspired by @labnol
// http://labnol.org/?p=27941
export function prepareVideo() {
    let div, n,
        v = document.getElementsByClassName("js-video-player");

    // Loop each video and perform each step
    for (n = 0; n < v.length; n++) {
        if(v[n].dataset) {
            div = document.createElement("div");
            div.setAttribute("data-id", v[n].dataset.id);
            div.setAttribute("data-host", v[n].dataset.host);
            div.innerHTML = getThumb(v[n].dataset.host, v[n].dataset.id, v[n].dataset.poster);
            div.onclick = setIframe;
            v[n].appendChild(div);
        }
    }

    // Fetch the thumbnails
    function getThumb(host, id, poster) {
        let play = '<div class="play"></div>';

        if (poster) {
            let thumb = '<img src="' + poster + '">';
            return thumb + play;
        } else if (host === 'youtube') {
            let thumb = '<img src="http://img.youtube.com/vi/ID/maxresdefault.jpg">';
            return thumb.replace("ID", id) + play;
        } else if (host === 'vimeo') {
            $.getJSON('https://www.vimeo.com/api/v2/video/' + id + '.json?callback=?', {format: "json"}, function (data) {
                let image = document.getElementById("img" + id);
                image.setAttribute("src", data[0].thumbnail_large);
            });
            return '<img id="img' + id + '">' + play;
        }
    }

    // Create the iframe and append it to the container
    function setIframe() {
        if (this.dataset.host === 'youtube') {
            let iframe = document.createElement("iframe");
            let embed = "https://www.youtube.com/embed/ID?autoplay=1&rel=0&amp;showinfo=0";
            iframe.setAttribute("src", embed.replace("ID", this.dataset.id));
            iframe.setAttribute("frameborder", "0");
            iframe.setAttribute("allowfullscreen", "1");
            this.parentNode.replaceChild(iframe, this);
        } else if (this.dataset.host === 'vimeo') {
            let iframe = document.createElement("iframe");
            let embed = "https://player.vimeo.com/video/ID?autoplay=1";
            iframe.setAttribute("src", embed.replace("ID", this.dataset.id));
            iframe.setAttribute("frameborder", "0");
            iframe.setAttribute("allowfullscreen", "1");
            this.parentNode.replaceChild(iframe, this);
        }
    }
}
