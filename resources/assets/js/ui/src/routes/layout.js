import routes from './core'


export default function(mode) {
    let route = routes(mode)

    appRegisterComponents({
        // 'data_blog_detail': 'DataBlogDetail.vue',
        // 'data_blog_overview': 'DataBlogOverview.vue',
        // 'data_blog_types_detail': 'DataBlogTypeDetail.vue',
        // 'data_blog_types_overview': 'DataBlogTypeOverview.vue',
        // 'data_form_contact_detail': 'DataFormContactDetail.vue',
        // 'data_form_contact_overview': 'DataFormContactOverview.vue',
        // 'data_form_newsletters_detail': 'DataFormNewsletterDetail.vue',
        // 'data_form_newsletters_overview': 'DataFormNewsletterOverview.vue',
    })

    return route
}
