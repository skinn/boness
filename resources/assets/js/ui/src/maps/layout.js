export default  (key) => {
    let module = null

    // if (key == 'DataBlogDetail.vue') return require('./../layout/app/blog/DataBlogDetail.vue')
    // if (key == 'DataBlogOverview.vue') return require('./../layout/app/blog/DataBlogOverview.vue')
    // if (key == 'DataBlogTypeDetail.vue') return  require('./../layout/app/blog/DataBlogTypeDetail.vue')
    // if (key == 'DataBlogTypeOverview.vue') return  require('./../layout/app/blog/DataBlogTypeOverview.vue')

    // if (key == 'DataFormContactDetail.vue') return require('./../layout/app/formContact/DataFormContactDetail.vue')
    // if (key == 'DataFormContactOverview.vue') return require('./../layout/app/formContact/DataFormContactOverview.vue')

    // if (key == 'DataFormNewsletterDetail.vue') return require('./../layout/app/formNewsletter/DataFormNewsletterDetail.vue')
    // if (key == 'DataFormNewsletterOverview.vue') return require('./../layout/app/formNewsletter/DataFormNewsletterOverview.vue')

    if (key == 'FrontOverview.vue') return require('./../layout/core/site/cms/FrontOverview.vue')
    if (key == 'FrontPagesOverview.vue') return require('./../layout/core/site/cms/FrontPagesOverview.vue')
    if (key == 'FrontMainMenu.vue') return require('./../layout/core/site/cms/FrontMainMenu.vue')

    if (key == 'Grid.vue') return require('./../layout/core/cms/core/grids/Grid.vue')
    if (key == 'DataTableBig.vue') return require('./../layout/core/cms/core/grids/DataTableBig.vue')
    if (key == 'DataTableMedium.vue') return require('./../layout/core/cms/core/grids/DataTableMedium.vue')

    if (key == 'MainMenuButton.vue') return require('./../layout/core/cms/core/menu/MainMenuButton.vue')

    if (key == 'Pager.vue') return require('./../layout/core/cms/core/pagers/Pager.vue')

    return module
}
