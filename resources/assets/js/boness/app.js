// -----------------------------------------------------------------------------
// Entrypoint for CMS Backoffice.
// -----------------------------------------------------------------------------

const boness = {
    init: function () {
        this.doSidebarNav()

        window.addEventListener('hashchange', () => {
          this.doSidebarNav();
        });
    },
    doSidebarNav: () => {
        const parents = document.querySelectorAll('.mainMenu__item');
        const children = document.querySelectorAll('.subMenu__link');
        const activeChild = document.querySelector('.subMenu__link.v-link-active');

        if (activeChild) {
            toggleClasses(activeChild);
        }

        function toggleClasses(el) {
            for(let i=0; i<parents.length; i++) {
              parents[i].classList.remove('v-link-active');
            }
        }

        for(let i=0; i<children.length; i++) {
          children[i].addEventListener('click', () => {
            toggleClasses(this);
          });
        }

        for(let i=0; i<parents.length; i++) {
          parents[i].addEventListener('click', () => {
            parents[i].classList.remove('v-link-active');
          });
        }
    }
};

document.addEventListener("DOMContentLoaded", function () {
    boness.init();
});
