// -----------------------------------------------------------------------------
//    _____    __ __    ____   _   __   _   __
//   / ___/   / //_/   /  _/  / | / /  / | / /
//   \__ \   / ,<      / /   /  |/ /  /  |/ /
//  ___/ /  / /| |   _/ /   / /|  /  / /|  /
// /____/  /_/ |_|  /___/  /_/ |_/  /_/ |_/
//
// -----------------------------------------------------------------------------
// This is your project's entrypoint. Fill it up!
// -----------------------------------------------------------------------------

// External Dependencies
import 'site/vendors';

// Internal Dependencies
import 'site/scripts/resize';
// import 'site/scripts/components.js';

// App
const app = {
    init: function () {

    }
}

app.init();
