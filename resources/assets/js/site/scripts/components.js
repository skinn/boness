import ComponentHandler from 'site/components/core/ComponentHandler';

const componentHandler = new ComponentHandler(
{
    // Example
    // 'maps': {
    //     'handler': require('site/components/maps/ComponentHandler').default,
    //     'components': {
    //         'map1': require('site/components/maps/map1/Component').default
    //     }
    // }
});

componentHandler.init();
