import { debounce } from 'vendor/skinn/core/Helpers';

document.addEventListener('DOMContentLoaded', function() {
    window.addEventListener('resize', debounce(onWindowResize, 250));
});

function onWindowResize() {
    // Do something on resize
}
