// Adaptive Images
import LazySizes from 'lazysizes';
import ObjectFit from 'lazysizes/plugins/object-fit/ls.object-fit.min.js';
import PictureFill from 'picturefill';
import Intrinsic from 'picturefill/src/plugins/intrinsic-dimension/pf.intrinsic.js';

// TweenMax
// import { TweenMax } from 'gsap/TweenMax';
// import ScrollToPlugin from 'gsap/ScrollToPlugin';
