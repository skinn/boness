<?php

use Illuminate\Database\Seeder;

class PageTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $json = '{ "results":{ "1":{ "id":1, "name":"contact", "label":"contact", "slug":"contact", "url":"\/contact", "is_dynamic":0, "is_home_page":0, "online":0, "view":"front.pages.contact", "parent_id":0, "position":2, "data_collections":[ ], "widgets":[ ], "created_at":null, "updated_at":null, "translations":{ "nl":{ "id":1, "page_id":1, "language":"nl", "slug":"contact", "url":"\/nl\/contact", "regex":"^nl\/contact$", "content":"", "view":"", "css":"", "js":"", "is_home":0, "online":0, "widgets":[ ], "created_at":null, "updated_at":null } } }, "2":{ "id":2, "name":"home", "label":"home", "slug":"home", "url":"\/", "is_dynamic":0, "is_home_page":1, "online":1, "view":"front.pages.home", "parent_id":0, "position":0, "data_collections":[ ], "widgets":[ ], "created_at":null, "updated_at":null, "translations":{ "nl":{ "id":2, "page_id":2, "language":"nl", "slug":"home-page", "url":"\/nl", "regex":"^nl$", "content":"", "view":"", "css":"", "js":"", "is_home":0, "online":0, "widgets":[ ], "created_at":null, "updated_at":null } } }, "3":{ "id":3, "name":"blog", "label":"blog", "slug":"blog", "url":"\/blog", "is_dynamic":0, "is_home_page":0, "online":0, "view":"front.pages.blog.overview", "parent_id":0, "position":1, "data_collections":[ { "id":2, "name":"blogs", "bindings":[ ] } ], "widgets":[ ], "created_at":null, "updated_at":null, "translations":{ "nl":{ "id":3, "page_id":3, "language":"nl", "slug":"blog", "url":"\/nl\/blog", "regex":"^nl\/blog$", "content":"", "view":"", "css":"", "js":"", "is_home":0, "online":"", "widgets":[ ], "created_at":null, "updated_at":null } } }, "5":{ "id":5, "name":"blog detail", "label":"blog detail","slug":"blog-detail", "url":"\/blog\/{blog-detail}", "is_dynamic":1, "is_home_page":0, "online":0, "view":"front.pages.blog.detail", "parent_id":3, "position":0, "data_collections":[{"id":3,"name":"blog","bindings":["{url:3}"],"itemsPerPage":""},{"id":4,"name":"blog-items","bindings":["{data:blog_id}"]},{"id":5,"name":"previous-blog","bindings":["{data:blog_date}","{data:blog_id}","{url:3}"]},{"id":6,"name":"next-blog","bindings":["{data:blog_date}","{data:blog_id}","{url:3}"]},{"id":2,"name":"sitemap:blog.slug","bindings":[]}], "widgets":[ ], "created_at":null, "updated_at":{ "date":"2017-01-26 15:23:19.000000", "timezone_type":3, "timezone":"UTC" }, "translations":{ "nl":{ "id":5, "page_id":5, "language":"nl", "slug":"blog-detail", "url":"\/nl\/blog\/{blog-detail}", "regex":"^nl\/blog([\/][^\/]+)$", "content":"", "view":"", "css":"", "js":"", "is_home":0, "online":"", "widgets":[ ], "created_at":null, "updated_at":null } } } }, "count":4, "order":[ 1, 2, 3, 5 ] }';
        $data = json_decode($json, true);
        foreach ($data['results'] as $record) {

            if ($record['id'] == 4)
                continue;

            $pageService = \Illuminate\Support\Facades\App::make(\Flyfish\Modules\Pages\Services\PageService::class);

            $record = $this->processPage($record, $pageService);

            $this->processDynamicPages($record);
            $this->processHomePage($record, $pageService);
            $this->processContactPage($record, $pageService);

        }//endforeach
    }

    /**
     * @param $record
     */
    private function processDynamicPages($record)
    {
        if( ! $record['is_dynamic'])
            return;

        $ids = [];
        foreach ($record['widgets'] as $widget)
            if (in_array($widget['alias'], ['metatags', 'page-title']))
                $ids[] = $widget['id'];

        $entityService = \Illuminate\Support\Facades\App::make(\Flyfish\Modules\Entity\Services\EntityService::class);
        foreach ($ids as $id) {
            $entity = $entityService->select($id);
            $entity['settings']->dynamic = true;
            $entityService->save($entity, $entity['id']);
        }//endforeach
    }

    /**
     * @param $pageService
     * @param $record
     * @return array
     */
    private function processHomePage($record, $pageService)
    {
        if( ! $record['is_home_page'])
            return;

        $pageService->linkWidgets($record, [['metatag-widget' => 'global-metatags', 'form-widget' => 'newsletter']], true, false);
        $entityService = \Illuminate\Support\Facades\App::make(\Flyfish\Modules\Entity\Services\EntityService::class);


        $entity = $entityService->findBySlug('global-metatags');
        foreach ($entity['content'] as $language => $content)
            $entity['content']->{$language} = json_decode('{"tags":[{"name": "description", "content": "", "category": "general", "type":"text", "editable": true, "label": "meta description", "chars": 155},{"name": "robots", "content": "", "category": "general", "type":"string", "editable": false, "label": "robots.txt", "chars": 0},{"name": "twitter:card", "content": "", "category": "twitter", "type":"text", "editable": true, "label": "twitter:card", "chars": 0},{"property": "og:title", "content": "", "category": "og", "type":"string", "editable": true, "label": "og:title", "chars": 0},{"property": "og:type", "content": "", "category": "og", "type":"string", "editable": true, "label": "og:type", "chars": 0},{"property": "og:url", "content": "{request:url}", "category": "og", "type":"string", "editable": true, "label": "og:url", "chars": 0},{"property": "og:description", "content": "", "category": "og", "type": "text", "editable": true, "label": "og:description", "chars": 0},{"property": "og:image", "content": "", "category": "og", "type": "image", "editable": true, "label": "og:image", "chars": 0}]}', true);

        $entityService->save($entity, $entity['id']);


        $entity = $entityService->findBySlug('newsletter');
        $entity['settings'] = json_decode('{"table":"data_form_newsletters","rules":{"email":"required|email"},"fields":[{"name":"email","type":"text","label":"email","value":""}],"mail_from":"develop@skinn.be","mail_to":"develop@skinn.be","mailchimp_email_field":"","view":"widgets.form.newsletter","script":"","panel":"data_form_newsletter_overview"}', true);

        $entityService->save($entity, $entity['id']);

    }

    /**
     * @param $record
     * @param $pageService
     * @return mixed
     */
    private function processContactPage($record, $pageService)
    {
        if ($record['slug'] == 'contact') {

            $record = $pageService->linkWidgets($record, [['form-widget' => 'contact-form']]);

            $entityService = \Illuminate\Support\Facades\App::make(\Flyfish\Modules\Entity\Services\EntityService::class);
            $entity = $entityService->findBySlug('contact-form');
            $entity['settings'] = json_decode('{"table":"data_form_contact","rules":{"email":"required|email","last_name":"required","first_name":"required","message":"required"},"fields":[{"name":"email","type":"text","label":"email","value":""},{"name":"message","type":"textarea","label":"description","value":""},{"name":"last_name","type":"text","label":"","value":""},{"name":"first_name","type":"text","label":"","value":""}],"mail_from":"develop@skinn.be","mail_to":"develop@skinn.be","view":"widgets.form.contact","script":"","panel":"data_form_contact_overview"}', true);
            $entity['label'] = "Contact formulier";

            $entityService->save($entity, $entity['id']);

            return $record;

        }//endif

        return $record;
    }

    /**
     * @param $record
     * @param $pageService
     * @return mixed
     */
    private function processPage($record, $pageService)
    {
        $translation = $record['translations']['nl'];
        unset($record['translations']);
        unset($record['created_at']);
        unset($record['updated_at']);

        unset($translation['created_at']);
        unset($translation['updated_at']);

        $translation['widgets'] = json_encode($translation['widgets']);
        $record['widgets'] = json_encode($record['widgets']);
        $record['data_collections'] = json_encode($record['data_collections']);

        $pageId = DB::table('core_pages')->insertGetId(
            $record
        );

        DB::table('core_pages_language')->insert(
            $translation
        );

        $record = $pageService->select($pageId);
        $record = $pageService->linkWidgets($record);

        return $record;
    }
}
