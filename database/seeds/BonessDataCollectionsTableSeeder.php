<?php

class BonessDataCollectionsTableSeeder extends DatabaseSeeder {


    public function run()
    {
        $results = DB::table('core_data_sources')->get();
        $dataSources = [];
        foreach($results as $result)
            $dataSources[$result->mapping] = $result->id;

        $records = array(
            array(
                'name' => 'blog overview',
                'label' => 'blog overview',
                'slug' => 'blog-overview',
                'query' => '{"select":' .  $dataSources['data_blog'] . ',"join":[],"and":[{"or":[{"left":"blog.online","operator":"=","right":1,"dynamic":false,"parentId":0,"lastIndex":0}]}],"alias":"blog","orderBy":[{"sort":"blog.date","direction":"desc"}],"random":0,"offset":{"take":""}}',
                'global' => 0
            ),
            array(
                'name' => 'blog detail',
                'label' => 'blog detail',
                'slug' => 'blog-detail',
                'query' => '{"select":' .  $dataSources['data_blog'] . ',"join":[],"and":[{"or":[{"left":"blog.slug","operator":"=","right":"","dynamic":true,"parentId":0,"lastIndex":0}]},{"or":[{"left":"blog.online","operator":"=","right":"1","dynamic":false,"parentId":1,"lastIndex":0}]}],"alias":"blog","orderBy":[],"random":0,"offset":{"take":1}}',
                'global' => 0
            ),
            array(
                'name' => 'blog items',
                'label' => 'blog items',
                'slug' => 'blog-items',
                'query' => '{"select":' .  $dataSources['data_blog_items'] . ',"join":[],"and":[{"or":[{"left":"blog_items.blog_id","operator":"=","right":"","dynamic":true,"parentId":0,"lastIndex":0}]}],"alias":"blog_items","orderBy":[{"sort":"blog_items.position","direction":"asc"}],"random":0,"offset":{"take":""}}',
                'global' => 0
            ),
            array(
                'name' => 'previous blog',
                'label' => 'previous blog',
                'slug' => 'previous-blog',
                'query' => '{"select":' .  $dataSources['data_blog'] . ',"join":[{"on":' .  $dataSources['data_blog_types'] . ',"as":"blog_type","left":"previous_blog.type","operator":"=","right":"blog_type.id"}],"and":[{"or":[{"left":"previous_blog.date","operator":">=","right":"","dynamic":true,"parentId":0,"lastIndex":0}]},{"or":[{"left":"previous_blog.id","operator":"<>","right":"","dynamic":true,"parentId":1,"lastIndex":0}]},{"or":[{"left":"previous_blog.slug","operator":"<>","right":"","dynamic":true,"parentId":2,"lastIndex":0}]},{"or":[{"left":"previous_blog.online","operator":"=","right":1,"dynamic":false,"parentId":3,"lastIndex":0}]}],"alias":"previous_blog","orderBy":[{"sort":"previous_blog.date","direction":"asc"},{"sort":"previous_blog.id","direction":"asc"}],"random":0,"offset":{"take":1}}',
                'global' => 0
            ),
            array(
                'name' => 'next blog',
                'label' => 'next blog',
                'slug' => 'next-blog',
                'query' => '{"select":' .  $dataSources['data_blog'] . ',"join":[{"on":' .  $dataSources['data_blog_types'] . ',"as":"blog_type","left":"next_blog.type","operator":"=","right":"blog_type.id"}],"and":[{"or":[{"left":"next_blog.date","operator":"<=","right":"","dynamic":true,"parentId":0,"lastIndex":0}]},{"or":[{"left":"next_blog.id","operator":"<>","right":"","dynamic":true,"parentId":1,"lastIndex":0}]},{"or":[{"left":"next_blog.slug","operator":"<>","right":"","dynamic":true,"parentId":2,"lastIndex":0}]},{"or":[{"left":"next_blog.online","operator":"=","right":1,"dynamic":false,"parentId":3,"lastIndex":0}]}],"alias":"next_blog","orderBy":[{"sort":"next_blog.date","direction":"desc"},{"sort":"next_blog.id","direction":"desc"}],"random":0,"offset":{"take":1}}',
                'global' => 0
            ),
        );

        foreach ($records as $record)
            DB::table('core_data_collections')->insert(
                $record
            );
    }
}