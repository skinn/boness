<?php

use Illuminate\Database\Seeder;

class TokensTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $tokens = array(
            array(
                'category' => 'general',
                'name' => 'Site name',
                'label' => 'Site naam',
                'slug' => 'site-name',
                'type' => 'text',
                'images' => '{"asset":[]}',
                'position' => 0
            ),
            array(
                'category' => 'general',
                'name' => 'Logo',
                'label' => 'Logo',
                'slug' => 'logo',
                'type' => 'image',
                'images' => '{"asset":[]}',
                'position' => 0
            ),
            array(
                'category' => 'general',
                'name' => 'Email',
                'label' => 'Email',
                'slug' => 'email',
                'type' => 'text',
                'images' => '{"asset":[]}',
                'position' => 0
            ),
            array(
                'category' => 'general',
                'name' => 'Address',
                'label' => 'Adres',
                'slug' => 'address',
                'type' => 'text',
                'images' => '{"asset":[]}',
                'position' => 0
            ),
            array(
                'category' => 'general',
                'name' => 'Postal Code',
                'label' => 'Postal Code',
                'slug' => 'postal-code',
                'type' => 'text',
                'images' => '{"asset":[]}',
                'position' => 0
            ),
            array(
                'category' => 'general',
                'name' => 'City',
                'label' => 'Gemeente',
                'slug' => 'city',
                'type' => 'text',
                'images' => '{"asset":[]}',
                'position' => 0
            ),
            array(
                'category' => 'general',
                'name' => 'Phone',
                'label' => 'Telefoon',
                'slug' => 'phone',
                'type' => 'text',
                'images' => '{"asset":[]}',
                'position' => 0
            ),
            array(
                'category' => 'social-media',
                'name' => 'Facebook',
                'label' => 'Facebook',
                'slug' => 'facebook',
                'type' => 'text',
                'images' => '{"asset":[]}',
                'position' => 0
            ),
            array(
                'category' => 'social-media',
                'name' => 'Twitter',
                'label' => 'Twitter',
                'slug' => 'twitter',
                'type' => 'text',
                'images' => '{"asset":[]}',
                'position' => 0
            ),
            array(
                'category' => 'social-media',
                'name' => 'Instagram',
                'label' => 'Instagram',
                'slug' => 'instagram',
                'type' => 'text',
                'images' => '{"asset":[]}',
                'position' => 0
            ),
            array(
                'category' => 'social-media',
                'name' => 'YouTube',
                'label' => 'YouTube',
                'slug' => 'youtube',
                'type' => 'text',
                'images' => '{"asset":[]}',
                'position' => 0
            ),
            array(
                'category' => 'social-media',
                'name' => 'Pinterest',
                'label' => 'Pinterest',
                'slug' => 'pinterest',
                'type' => 'text',
                'images' => '{"asset":[]}',
                'position' => 0
            ),
            array(
                'category' => 'social-media',
                'name' => 'LinkedIn',
                'label' => 'LinkedIn',
                'slug' => 'linkedin',
                'type' => 'text',
                'images' => '{"asset":[]}',
                'position' => 0
            ),
            array(
                'category' => 'seo',
                'name' => 'Google Tag Manager ID',
                'label' => 'Google Tag Manager ID',
                'slug' => 'google-tag-manager-id',
                'type' => 'text',
                'images' => '{"asset":[]}',
                'position' => 0
            ),
        );

        foreach ($tokens as $token)
            DB::table('core_tokens')->insert(
                $token
            );
    }
}
