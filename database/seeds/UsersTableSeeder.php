<?php

class UsersTableSeeder extends DatabaseSeeder {


    public function run()
    {
        $users = array(
            array(
                'name' => 'admin',
                'email' => 'admin@skinn.be',
                'password' => '$2y$10$LhvE9yy.0VEozNHkjUhOseUhn0yhhAGfqtSNA0o3ilq9U.9FLtYui'
            )
        );

        foreach ($users as $user) {
            $id = DB::table('users')->insertGetId($user);
            DB::table('core_role_user')->insert(
                array(
                    'role_id' => 1,
                    'user_id' => $id
                )
            );

        }

    }
}