# Skinn/Boness

This repository contains the layout and graphics for skinnCMS.

You need both [`flyfish/core`](https://bitbucket.org/skinn/cms-core) and [`skinn/bones`](https://bitbucket.org/skinn/boness) to build and use the CMS

## Contents

TBD

## Installation

The recommended option is to use the generator. Follow the instructions on [npmjs.org](https://www.npmjs.com/package/generator-boness).

If you're not able to use it, try following the [alternative step-by-step guide](https://bitbucket.org/skinn/boness-generator/src/master/BACKUP.md).