# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](http://keepachangelog.com/)
and this project adheres to [Semantic Versioning](http://semver.org/).

## [unreleased] - yyyy-mm-dd
### added / changed / fixed / ...

## [2.6.2] - 2019-06-03
### fixed
- Fix: remove console.log from scrollto

## [2.6.1] - 2019-06-03
### fixed
- Fix: set active multiple source links (for instance seperate regular menu & sticky menu)

## [2.6] - 2019-06-03
### changed
- Add new settings to SlideOpen
- Add new settings to ScrollTo
- remove deprecated JavaScript classes
- Add comments to ScrollTo class
- Update comments to SlideOpen class
- SocialShare class added
- Add comments to SocialShare class

## [2.5] - 2019-05-13
### changed
- Enable autoscroll on drag in sortable lists
- Fix empty UL on when rendering empty menu item. Do not render parent tag. 
- Fix paging when initializing page overview
- Remove waiting page blade and master 
- Remove display:block from image tags in website css. Causes Google maps icons to break

## [2.4.1] - 2019-03-11
### fixed
- Fixed GSAP import issue

## [2.4] - 2018-09-26
### changed
- Added default 404 page

## [2.3.1] - 2018-05-31
### fixed
- Revert grid wrap changes

## [2.3] - 2018-05-28
### added
- Add default disclaimer and privacypolicy pages
- Add default cookie banner
- Add responsive custom gaps to grid
- Add class to only render vertical or horizontal gaps
- Add seperate vendors for backoffice (jQuery)

### changed
- Update core JS classes
- Mark old core JS classes as deprecated
- Update default datasources
- Update reboot.scss
- Clean and remove old CSS
- Reorganized app.js and other files
- Replaced skinn logo
- Refactored CMS core files
- Removed jQuery and dependencies

### fixed
- Fix loading Vimeo thumbnail
- Fix error when sending form mail with fields that don't exist
- Fix wrong prev/next buttons on article detail.
- Fix major visual bugs in IE11
- Fix minor visual bugs in modern browsers
- Fix max height on accordion that caused overflow on long texts

## [2.2] - 2018-02-06
### added
- Custom language file (custom.php) for all custom translations.
- Custom text sizes are available in `_variables.scss`.
### changed
- Updated Grid to include vertical flow (again) and minor optimisations.
- Updated reboot.scss from Bootstrap.
- Removed demo from default installation.
- Styled default mail templates when sending forms.

## [2.1.3] - 2018-01-09
### fixed
- fix: reboot added margin-bottom (= issue in texteditor)

## [2.1.2] - 2018-01-09
### changed
- update default install html to new grid features

## [2.1.1] - 2018-01-09
### added
- feature: js classes for sticky header & reveal header added

## [2.1.0] - 2017-12-22
### changed
- Updated from grid v2 to grid v3
- Updated normalize to reboot
- Minor changes to sass structure

### dependencies
- flyfish 4.1.*

## [2.0.1] - 2017-12-21
### added
- fix: ie10 error for preparevideo method

## [2.0.0] - 2017-12-21
### added
- implementing db gui
- adding export/import

### dependencies
- flyfish 4.0.*

## [1.4.6] - 2017-09-27
### fixed
- fix: negative value offset for scrolldetector helper function

## [1.4.5] - 2017-08-21
### fixed
- hotfix: change IE check to include older IE versions (<= 10)

## [1.4.4] - 2017-08-17
### fixed
- adding removed form__field-medium-editor--collapse-paragraphs class for mediumeditor

## [1.4.3] - 2017-08-08
### fixed
- Grid (Corpus)

## [1.4.2] - 2017-07-31
### fixed
- Grid (Corpus)

## [1.4.1] - 2017-07-17
### fixed
- Helpers.js (Corpus)

## [1.4.0] - 2017-07-13
### added
- corpus
- dashboard
- layout backoffice

### dependencies
- core 3.4.*

## [1.3.11] - 2017-07-07
### fixed
- ImageUpload loading spinner css added

## [1.3.10] - 2017-07-07
### fixed
- Imageloader dispatching check take two.

## [1.3.9] - 2017-07-07
### fixed
- Imageloader dispatching check.

### added
- Added new percentual flexbox grid classes (e.g. square, half, etc..)
- ScrollTo JS class
- getOffset method in Helpers.js

### changed
- Refined typography rules
- Default editor styles are now applied to .c-text instead of .site-wrapper
- Moved site padding to .c-container instead of .site-wrapper
- Renamed some flexbox grid classes (.col--full, row--full, etc..)

### removed
- Removed prefixes from all project specific Sass

## [1.3.8] - 2017-06-23
### fixed
- added missing CSS for 'copy' icon.

## [1.3.7] - 2017-06-23
### added
- added 'copy' icon to sprite.

## [1.3.6] - 2017-06-20
### fixed
- issue jquery vendors in front

## [1.3.5] - 2017-06-20
### added
- changing date to dateTime
- using current timestam as default when creating new blog
- adding require moment in blog detail

## [1.3.4] - 2017-06-08
### added
- change default value global meta robots to empty + typo fix robot -> robots

## [1.3.3] - 2017-06-08
### added
- re-push medium editor padding change

## [1.3.2] - 2017-06-08
### added
- Fixed inheritance issue with global typography rules.

## [1.3.1] - 2017-06-02
### added
- adding time to date component

## [1.3.0] - 2015-05-30
### added
- back office: settings: overview/detail (seo) redirect
- back office: settings: overview/detaillanguages
- back office: users: overview/detail
- migration refactor + removing prefix module
- paragraph style for medium editor
- file upload spinner
- Added sourcemaps to CSS files
- adding page menu blades
- adding blog menu blades

### changed
- Slightly cleaned app.js
- removing og:card in MetaTags + adding default value 'website' to og:type
- Restructured Core CSS.
    - Grid: Added offset classes per breakpoint.
    - Grid: Boness now has its own grid structure, separate from the site.
- Reorganized Skinn Core CSS and named it Corpus.
- Slighty altered e-mail templates.
- Slighty altered translatables.
- Restructured master.blade.php in in /resources/views/front.
- Replaced old CSS classes in CMS source files.

### fixed
- Fixed loading vendors for app.js

### changed
- replacing text-editor with text-widget-item in FrontOverview

## [1.2.8] - 2017-05-23
### added
- imageloader js class rewritten, preloader js class rewritten

## [1.2.7] - 2017-05-12
### fixed
- multiple images in front office  not working when checkbox "use multiple" was checked in asset entity

## [1.2.6] - 2017-05-03
### fixed
- removing double page title in frontoverview

## [1.2.5] - 2017-05-02
### added
- CheckboxWidget

## [1.2.4] - 2017-04-26
### fixed
- adding language field in front form detail.vue
- adding sort field in defaultQuery of front form overview.vue
- copying lang folder if groups.php not exists
- creating src/ui/app folder if not exists

## [1.2.3] - 2017-04-24
### fixed
- show translatable images in frontOverview

## [1.2.2] - 2017-04-14
### changed
- removing unused boness_fields mapping
- when publishingen don't copy lang files if already exists
- removing double datalist in core migrations

## [1.2.1] - 2017-04-06
### changed
- bugfix submenu + send params when emitting events

## [1.2.0] - 2017-04-05
### added
- Added a default homepage
- Adding metatag and pageTitle to front office
- Updating seed files
- Adding group file in resource/lang
- Copying front and widgets views + resource/lang files with publish command
- Adding $pages for front, containing all pages fe for menus
- Adding mail template functionality + translations
- Adding og:image:width and og:image:height
- Added ability to import external css files from node_modules sass/vendor/vendors

### changed
- Adjusted folder structure: /resources/views/front
- /resources/views/front templates are now published as well, but only the first time.
- Removed $layout from templates.
- Removing seo tokens and adding site name token
- Cleaning up language files
- Rewrote datalists templates from scratch.
- Refactoring pageSeeder
- Cleaning up the front blade files
- Updating pagination translations

### removed
- bonessFields components

## [1.1.1] - 2017-03-27
### added
- mouseover event sometimes originates from child object, causing the submenu not to open
- possibility to execute scroll animations only once

## [1.1.0] - 2017-03-20
### changed
- updating /Skinn/Boness/Commands/PublishBoness.php
- changing folder structure: /resources/assets
- cleaning up folder structure: /resources/views

## [1.0.0] - 2017-03-08
### added
- first version boness package
