<?php namespace Skinn\Boness\Controllers;

use Flyfish\Core\Controllers\FlyFishController;
use Flyfish\Modules\Auth\Facades\AuthService;


class AdminController extends FlyfishController {


	/**
	 * Show the application dashboard to the user.
	 *
	 * @return Response
	 */
	public function index()
	{


        $go = false;
        foreach(AuthService::roles() as $role)
            if($role['id'] == 1)
               $go = true;

        if( ! $go)
            return redirect('/#!/admin');

		if(view()->exists('back.dashboard'))
			return view('back.dashboard', array('csrf' => csrf_token()));
    
		return view('skinn-boness::back.dashboard', array('csrf' => csrf_token()));
	}

}
