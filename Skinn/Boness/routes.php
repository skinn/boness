<?php

$lang = \Flyfish\Modules\Languages\Facades\LanguageManager::getCurrentLanguage(LanguageManager::C_TYPE_FRONT);
if (!$lang)
    $lang = 'nl';

Route::group(['middleware' => 'web'], function () use ($lang) {
    Route::group(array('prefix' => 'admin', 'middleware' => 'auth'), function () {
        Route::get('/', 'Skinn\Boness\Controllers\AdminController@index');
        Route::get('{all?}', function($all){
            return redirect('/admin#!/admin/' . $all);
        });
    });
});
