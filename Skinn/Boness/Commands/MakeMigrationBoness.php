<?php

namespace Skinn\Boness\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\Artisan;

class MakeMigrationBoness extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'boness:make:migration {name} {--create=} {--table=}';


    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'boness:make:migration {name} {--create=} {--table=}';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {


        $options = ['--path' => 'vendor/skinn/boness/database/migrations'];
        $create = $this->option('create');
        if($create)
            $options['--create'] = $create;

        $table = $this->option('table');
        if($table)
            $options['--table'] = $table;

        $name = $this->argument('name');
        if($name)
            $options['name'] = $name;

        Artisan::call('make:migration',  $options);

    }
}
