<?php

namespace Skinn\Boness\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\Artisan;
use Illuminate\Support\Facades\File;

class PublishBoness extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'boness:publish {--force}';
    protected $name = 'boness:publish';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'boness:publish (--force)';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $this->info('Publishing skinn/boness...');
        Artisan::call('flyfish:publish', array('namespace' => 'skinn', '--ui' => 'skinn/boness'));

        $toCopy = $this->getPathsToCopy('skinn/boness');
        $toRemove = $this->getPathsToRemove();

        foreach ($toCopy as $path) {
            $source = $path['source'];
            $target = $path['target'];

            if (File::exists($source)) {
                File::copyDirectory($source, $target);
            }
        }

        foreach ($toRemove as $path) {
            $target = $path['target'];

            if (File::exists($target)) {
                File::deleteDirectory($target);
            }
        }

    }

    private function getPathsToCopy($ui)
    {
        if (!File::exists(base_path('resources/assets/js/ui/src/app')))
            File::makeDirectory(base_path('resources/assets/js/ui/src/app'));

        if (!File::exists(base_path('resources/lang/groups.php')))
            File::deleteDirectory(base_path('resources/lang'));

        // Items that always need to be updated & copied.
        $list = [
            ['source' => base_path('vendor/' . $ui . '/resources/assets/fonts'), 'target' => resource_path('assets/fonts')],
            ['source' => base_path('vendor/' . $ui . '/resources/assets/icons'), 'target' => resource_path('assets/icons')],
            ['source' => base_path('vendor/' . $ui . '/resources/assets/images'), 'target' => resource_path('assets/images')],
            ['source' => base_path('vendor/' . $ui . '/resources/assets/js/boness'), 'target' => resource_path('assets/js/boness')],
            ['source' => base_path('vendor/' . $ui . '/resources/assets/js/common'), 'target' => resource_path('assets/js/common')],
            ['source' => base_path('vendor/' . $ui . '/resources/assets/js/ui'), 'target' => resource_path('assets/js/ui')],
            ['source' => base_path('vendor/' . $ui . '/resources/assets/js/vendor'), 'target' => resource_path('assets/js/vendor')],
            ['source' => base_path('vendor/' . $ui . '/resources/assets/sass/boness'), 'target' => resource_path('assets/sass/boness')],
            ['source' => base_path('vendor/' . $ui . '/resources/assets/sass/common'), 'target' => resource_path('assets/sass/common')],
            ['source' => base_path('vendor/' . $ui . '/database'), 'target' => base_path('database')],
        ];

        // Items that don't need to be updated and are custom for each project (unless forced option is given)
        $checkList = [
            ['source' => base_path('vendor/' . $ui . '/resources/assets/sass/site'), 'target' => resource_path('assets/sass/site')],
            ['source' => base_path('vendor/' . $ui . '/resources/assets/js/site'), 'target' => resource_path('assets/js/site')],
            ['source' => base_path('vendor/' . $ui . '/resources/views/front'), 'target' => resource_path('views/front')],
            ['source' => base_path('vendor/' . $ui . '/resources/views/widgets'), 'target' => resource_path('views/widgets')],
            ['source' => base_path('vendor/' . $ui . '/resources/views/mails'), 'target' => resource_path('views/mails')],
            ['source' => base_path('vendor/' . $ui . '/resources/lang'), 'target' => resource_path('lang')],
            ['source' => base_path('vendor/' . $ui . '/modules/import'), 'target' => base_path('modules/import')],
        ];

        foreach ($checkList as $key => $path) {
            $target = $path['target'];

            $force = $this->option('force');

            if ($force || $this->checkDoubles($target)) {
                $list[] = $checkList[$key];
            }
        }

        return $list;
    }

    private function checkDoubles($target)
    {
        if (!File::exists($target)) {
            return true;
        } else {
            $this->info($target . ' already exists. Skipping...');
            return false;
        }
    }

    // Items that need to removed after installation.
    // These items will be loaded from the vendor files.
    private function getPathsToRemove()
    {
        $list = [
            ['target' => resource_path('views/front/cms')],
            ['target' => resource_path('views/front/default')],
            ['target' => resource_path('views/front/meta')]
        ];

        return $list;
    }
}
