<?php

namespace Skinn\Boness;


use Flyfish\Core\ServiceProviders\AbstractServiceProvider;
use Skinn\Boness\Commands\MakeMigrationBoness;
use Skinn\Boness\Commands\PublishBoness;
use Skinn\Ip\SkinnIpProvider;

class SkinnBonessServiceProvider extends AbstractServiceProvider
{
    protected $assets = [];
    protected $namespace = 'skinn';
    protected $packageName = 'boness';
    protected $dir = __DIR__;

    /* Register the application services.
    *
    * @return void
    */
    public function register()
    {
        parent::register();
        $this->commands(PublishBoness::class);
        $this->commands(MakeMigrationBoness::class);

        $class = '\\' . SkinnIpProvider::class;
        if (class_exists($class))
            $this->app->register($class);
    }
}
